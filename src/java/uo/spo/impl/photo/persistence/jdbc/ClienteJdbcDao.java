package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.persistence.ClienteDao;

public class ClienteJdbcDao implements ClienteDao {

	@Override
	public Cliente validateLogin(Cliente cliente) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select u.id, u.nombre, u.email,"
					+ " u.apellidos, u.created  from Usuario u inner join "
					+ "Cliente c on c.id = u.id  " + "where u.login = '"
					+ cliente.getLogin() + "' and u.password = '"
					+ cliente.getPassword() + "'");

			rs = ps.executeQuery();

			Cliente cliente_logueado = new Cliente();

			if (rs.next()) {
				cliente_logueado.setId(rs.getLong("id"));
				cliente_logueado.setNombre(rs.getString("nombre"));
				cliente_logueado.setEmail(rs.getString("email"));
				cliente_logueado.setApellidos(rs.getString("apellidos"));
				cliente_logueado.setCreated(rs.getDate("created"));
			}

			return cliente_logueado;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Cliente findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			Cliente usuario = new Cliente();

			ps = con.prepareStatement("select id, apellidos, nombre, created, "
					+ "zip, calle, ciudad, email, login  from "
					+ "Usuario u inner join Cliente c on c.id = u.id "
					+ "where id = " + id);
			rs = ps.executeQuery();

			if (rs.next()) {
				usuario.setNombre(rs.getString("nombre"));
				usuario.setEmail(rs.getString("email"));
				usuario.setApellidos(rs.getString("apellidos"));
				usuario.setCreated(rs.getDate("created"));
				usuario.setLogin(rs.getString("login"));
				usuario.setId(rs.getLong("id"));
			}

			ps = con.prepareStatement("select e.id, e.created, e.nombre, "
					+ "e.fotografo_id "
					+ "from evento e inner join usuario u on e.cliente_id ="
					+ " u.id where u.id = " + id);
			rs = ps.executeQuery();

			if (rs.next()) {
				Evento evento = new Evento();
				evento.setCliente(usuario);
				evento.setCreated(rs.getDate("created"));
				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				usuario.addEvento(evento);
			}

			ps = con.prepareStatement("select a.id, a.created, a.nombre "
					+ "from album a inner join usuario u on a.cliente_id = "
					+ "u.id where u.id = " + id);
			rs = ps.executeQuery();

			if (rs.next()) {
				Album album = new Album();
				album.setCliente(usuario);
				album.setCreated(rs.getDate("created"));
				album.setId(rs.getLong("id"));
				album.setNombre(rs.getString("nombre"));
				usuario.addAlbum(album);
			}

			return usuario;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Cliente> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Cliente> usuarios = new Vector<Cliente>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, apellidos, nombre, created,"
					+ " zip, calle, ciudad, email, login"
					+ "  from Usuario u inner join Cliente c on c.id = u.id");
			rs = ps.executeQuery();

			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setNombre(rs.getString("nombre"));
				cliente.setEmail(rs.getString("email"));
				cliente.setApellidos(rs.getString("apellidos"));
				cliente.setCreated(rs.getDate("created"));
				cliente.setLogin(rs.getString("login"));
				cliente.setId(rs.getLong("id"));
				usuarios.add(cliente);
			}

			return usuarios;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Cliente persist(Cliente entity) {
		return null;
	}

	@Override
	public Cliente merge(Cliente cliente) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("UPDATE Usuario SET nombre = '"
					+ cliente.getNombre() + "'" + ", apellidos = '"
					+ cliente.getApellidos() + "' where id = "
					+ cliente.getId());
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return cliente;
	}

	@Override
	public void remove(Cliente entity) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void lock(Cliente entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Cliente entity) {
		// TODO Auto-generated method stub

	}

}
