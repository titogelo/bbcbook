package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.AlbumFilterPhoto;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.AlbumDao;

public class AlbumJdbcDao implements AlbumDao {

	PreparedStatement ps = null;
	Connection con = null;

	@Override
	public Long countNumberOfFotografiasInAlbum(Long albumId) throws Exception {

		try {
			con = TransactionControl.getCurrentConnection();

			ps = con.prepareStatement("select count(*) as suma from"
					+ " fotografia_album where albums_id = " + albumId);
			ps.executeQuery();

			AlbumFilterPhoto album = new AlbumFilterPhoto("");

			return (long) album.getNumeroDeFotografias();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void updateEventAndAlbum(Long albumId) throws Exception {
		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("update album set nombre = 'modificacion de album'"
					+ " where albums_id = " + albumId);
			ps.executeQuery();

			ps = con.prepareStatement("update evento set nombre = 'modificacion de evento'"
					+ " where eventos_id = " + albumId);
			ps.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Long fotografiasMediaPorAlbum() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("select count(fotografias_id)/count( "
					+ "distinct albums_id) as media from fotografia_album");
			rs = ps.executeQuery();

			AlbumFilterPhoto album = null;

			while (rs.next()) {

				album = new AlbumFilterPhoto("");
			}

			return (long) album.getNumeroDeFotografias();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}
		}
	}

	/**
	 * En este metodo se realizan consultas agregadas.
	 * 
	 * Se utilizan para que una
	 * 
	 * */
	@Override
	public List<AlbumFilterPhoto> albumsByNumFotografias(Long num_fotografias)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<AlbumFilterPhoto> albums = new Vector<AlbumFilterPhoto>();

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("select album.nombre, "
					+ "count(f.fotografias_id) "
					+ "from fotografia_album f inner join album on album.id = "
					+ "f.albums_id  " + "group by album.nombre "
					+ "having count(fotografias_id) >= " + num_fotografias);
			rs = ps.executeQuery();

			AlbumFilterPhoto album = null;

			while (rs.next()) {
				album = new AlbumFilterPhoto(rs.getString("nombre"));
				albums.add(album);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}
		}
		return albums;
	}

	@Override
	public void remove(Album entity) throws Exception {
		PreparedStatement ps = null;

		Connection con = null;

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("delete from Album where id = "
					+ entity.getId());
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}
		}

	}

	@Override
	public Album findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("select id, nombre, created, cliente_id  "
					+ "from Album where id = " + id);
			rs = ps.executeQuery();

			Album album = new Album();

			while (rs.next()) {
				album.setId(rs.getLong("id"));
				album.setNombre(rs.getString("nombre"));
				album.setCreated(rs.getDate("created"));

				ClienteJdbcDao cliente = new ClienteJdbcDao();
				album.setCliente(cliente.findById(rs.getLong("cliente_id")));
			}

			ps = con.prepareStatement("select f.id, f.created, f.nombre  "
					+ "from Album a left join fotografia_album fa on "
					+ "fa.albums_id = a.id "
					+ "left join fotografia f on fa.fotografias_id = f.id "
					+ "where a.id = " + id);
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografia fotografia = new Fotografia();
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
				album.addFotografia(fotografia);
			}

			return album;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Album> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Album> albums = new Vector<Album>();

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("select id, nombre, created, cliente_id"
					+ "  from Album");
			rs = ps.executeQuery();

			while (rs.next()) {
				Album album = new Album();
				album.setId(rs.getLong("id"));
				album.setNombre(rs.getString("nombre"));
				album.setCreated(rs.getDate("created"));

				ClienteJdbcDao cliente = new ClienteJdbcDao();
				album.setCliente(cliente.findById(rs.getLong("cliente_id")));

				albums.add(album);
			}

			return albums;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}

		}
	}

	@Override
	public Album persist(Album album) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			con = TransactionControl.getConnection();

			ps = con.prepareStatement("INSERT INTO Album ("
					+ "nombre, num_fotos, version" + ") VALUES (" + "'"
					+ album.getNombre() + "',0,0)");
			ps.executeUpdate();

			ps = con.prepareStatement("select max(id) as id" + "  from Album");
			rs = ps.executeQuery();

			@SuppressWarnings("unused")
			long max_id = 0;

			if (rs.next()) {
				album.setId(rs.getLong("id"));
			}

			return album;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();

			} catch (Exception e) {
			}
		}

	}

	@Override
	public Album merge(Album entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void lock(Album entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Album entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Album> findAlbumByCliente(Long clienteId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
