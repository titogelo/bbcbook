package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.EventoDao;

public class EventoJdbcDao implements EventoDao {

	@Override
	public Evento findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created,"
					+ " cliente_id, fotografo_id  " + "from Evento where id = "
					+ id);
			rs = ps.executeQuery();

			Evento evento = new Evento();
			while (rs.next()) {

				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				evento.setCreated(rs.getDate("created"));

				ClienteJdbcDao cliente = new ClienteJdbcDao();
				evento.setCliente(cliente.findById(rs.getLong("cliente_id")));

				FotografoJdbcDao fotografo = new FotografoJdbcDao();
				evento.setFotografo(fotografo.findById(rs
						.getLong("fotografo_id")));
			}

			ps = con.prepareStatement("select f.id, f.created, f.nombre  "
					+ "from Evento e inner join fotografia_evento fe on "
					+ "fe.eventos_id = e.id "
					+ "inner join fotografia f on fe.fotografias_id = f.id "
					+ "where e.id = " + id);
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografia fotografia = new Fotografia();
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
				evento.addFotografia(fotografia);
			}

			return evento;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Evento> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Evento> eventos = new Vector<Evento>();

		try {
			/*
			 * jdbcDataSource dataSource = new jdbcDataSource();
			 * dataSource.setDatabase("jdbc:hsqldb:hsql://localhost");
			 * 
			 * Connection conn = dataSource.getConnection("SA", "");
			 */
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created, cliente_id, "
					+ "fotografo_id from Evento");
			rs = ps.executeQuery();

			while (rs.next()) {
				Evento evento = new Evento();
				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				evento.setCreated(rs.getDate("created"));

				ClienteJdbcDao cliente = new ClienteJdbcDao();
				evento.setCliente(cliente.findById(rs.getLong("cliente_id")));

				FotografoJdbcDao fotografo = new FotografoJdbcDao();
				evento.setFotografo(fotografo.findById(rs
						.getLong("fotografo_id")));

				eventos.add(evento);
			}

			return eventos;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Evento persist(Evento evento) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("INSERT INTO Evento ("
					+ "nombre, cliente_id, fotografo_id, version"
					+ ") VALUES (" + "'" + evento.getNombre() + "',"
					+ evento.getCliente().getId() + ","
					+ evento.getFotografo().getId() + ",0)");
			ps.executeUpdate();

			ps = con.prepareStatement("select max(id) as id" + "  from Evento");
			rs = ps.executeQuery();

			@SuppressWarnings("unused")
			long max_id = 0;

			if (rs.next()) {
				evento.setId(rs.getLong("id"));
			}

			return evento;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Evento merge(Evento evento) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("UPDATE Evento SET nombre = '"
					+ evento.getNombre() + "' where id = " + evento.getId());
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return evento;
	}

	@Override
	public void remove(Evento entity) throws Exception {
		PreparedStatement ps = null;

		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("delete * from Evento where id = "
					+ entity.getId());
			ps.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void lock(Evento entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Evento entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Evento> getEventosByFotografo(Long fotografoId)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Evento> eventos = new ArrayList<Evento>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created  from Evento "
					+ "where fotografo_id = " + fotografoId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Evento evento = new Evento();
				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				evento.setCreated(rs.getDate("created"));
				eventos.add(evento);
			}

			return eventos;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Evento> findEventosByCliente(Long clienteId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Evento> eventos = new ArrayList<Evento>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created  from Evento"
					+ " where cliente_id = " + clienteId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Evento evento = new Evento();
				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				evento.setCreated(rs.getDate("created"));
				eventos.add(evento);
			}

			return eventos;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

}
