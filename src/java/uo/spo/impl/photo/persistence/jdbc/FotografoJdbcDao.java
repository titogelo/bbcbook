package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.persistence.FotografoDao;

public class FotografoJdbcDao implements FotografoDao {

	@Override
	public Fotografo validateLogin(Fotografo fotografo) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select u.id, u.nombre, u.email,"
					+ " u.apellidos, u.created, u.login from Usuario u inner "
					+ "join Fotografo c on c.id = u.id  " + "where u.login = '"
					+ fotografo.getLogin() + "' and u.password = '"
					+ fotografo.getPassword() + "'");

			rs = ps.executeQuery();

			Fotografo fotografo_logueado = new Fotografo();

			if (rs.next()) {
				fotografo_logueado.setId(rs.getLong("id"));
				fotografo_logueado.setNombre(rs.getString("nombre"));
				fotografo_logueado.setEmail(rs.getString("email"));
				fotografo_logueado.setApellidos(rs.getString("apellidos"));
				fotografo_logueado.setCreated(rs.getDate("created"));
				fotografo_logueado.setLogin(rs.getString("login"));
			}

			return fotografo_logueado;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Fotografo findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			Fotografo usuario = new Fotografo();

			ps = con.prepareStatement("select id, apellidos, nombre, created, "
					+ "zip, calle, ciudad, email, login  from "
					+ "Usuario u inner join Fotografo c on c.id = u.id "
					+ "where id = " + id);
			rs = ps.executeQuery();

			if (rs.next()) {
				usuario.setId(rs.getLong("id"));
				usuario.setNombre(rs.getString("nombre"));
				usuario.setEmail(rs.getString("email"));
				usuario.setApellidos(rs.getString("apellidos"));
				usuario.setCreated(rs.getDate("created"));
				usuario.setLogin(rs.getString("login"));
			}

			ps = con.prepareStatement("select e.id, e.created, e.nombre,"
					+ " e.fotografo_id "
					+ "from evento e inner join usuario u on e.fotografo_id = "
					+ "u.id where u.id = " + id);
			rs = ps.executeQuery();

			if (rs.next()) {
				Evento evento = new Evento();
				evento.setCreated(rs.getDate("created"));
				evento.setId(rs.getLong("id"));
				evento.setNombre(rs.getString("nombre"));
				usuario.addEvento(evento);
			}

			return usuario;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Fotografo> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Fotografo> usuarios = new Vector<Fotografo>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, apellidos, nombre, created, "
					+ "zip, calle, ciudad, email, login"
					+ "  from Usuario u inner join Fotografo c on c.id = u.id");
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografo fotografo = new Fotografo();
				fotografo.setNombre(rs.getString("nombre"));
				fotografo.setEmail(rs.getString("email"));
				fotografo.setApellidos(rs.getString("apellidos"));
				fotografo.setCreated(rs.getDate("created"));
				fotografo.setLogin(rs.getString("login"));
				fotografo.setId(rs.getLong("id"));
				usuarios.add(fotografo);
			}

			return usuarios;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Fotografo persist(Fotografo fotografo) throws Exception {
		return fotografo;
	}

	@Override
	public Fotografo merge(Fotografo fotografo) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("UPDATE Usuario SET nombre = '"
					+ fotografo.getNombre() + "', apellidos = '"
					+ fotografo.getApellidos() + "' where id = "
					+ fotografo.getId());
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return fotografo;
	}

	@Override
	public void remove(Fotografo entity) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void lock(Fotografo entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Fotografo entity) {
		// TODO Auto-generated method stub

	}

}
