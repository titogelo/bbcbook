package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uo.spo.impl.photo.business.commands.Command;
import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.photo.infrastructure.Factories;

/**
 * CommandoExecutor para JPA, si necesitas cambiar alguna capa de persistencia,
 * es necesario cambiar cosas aqui. Esta clase se encarga de: - Gestionar el
 * EntityManager para cada hilo - Manejar la transacción.
 */
public class JdbcCommandExecutor implements CommandExecutor {
	private static Logger log = LoggerFactory
			.getLogger(JdbcCommandExecutor.class);

	@Override
	public Object execute(Command c) throws Exception {
		Object res = null;
		Connection connection = null;

		try {
			connection = TransactionControl.getCurrentConnection();

			connection.setAutoCommit(false);

			c.setDAOFactory(Factories.persistence);

			res = c.execute();

			connection.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
				log.debug("rollback by exception in " + e.getClass().getName(),
						e);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
		return res;
	}
}
