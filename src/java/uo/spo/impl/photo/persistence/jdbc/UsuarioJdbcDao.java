package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.model.Usuario;
import uo.spo.photo.persistence.UsuarioDao;

public class UsuarioJdbcDao implements UsuarioDao {

	@Override
	public Usuario findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "amazin");

			ps = con.prepareStatement("select id, apellidos, nombre, created, zip, calle, ciudad, email, login  from "
					+ "Usuario u inner join Fotografo c on c.id = u.id "
					+ "where id = " + id);
			rs = ps.executeQuery();

			Usuario usuario = null;

			if (rs.next()) {

				usuario = new Fotografo();

				usuario.setNombre(rs.getString("nombre"));
				usuario.setEmail(rs.getString("email"));
				usuario.setApellidos(rs.getString("apellidos"));
				usuario.setCreated(rs.getDate("created"));
			} else {
				ps = con.prepareStatement("select id, apellidos, nombre, created, zip, calle, ciudad, email, login  from "
						+ "Usuario u inner join Cliente c on c.id = u.id "
						+ "where id = " + id);
				rs = ps.executeQuery();

				if (rs.next()) {

					usuario = new Cliente();

					usuario.setNombre(rs.getString("nombre"));
					usuario.setEmail(rs.getString("email"));
					usuario.setApellidos(rs.getString("apellidos"));
					usuario.setCreated(rs.getDate("created"));
				}

			}

			return usuario;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Usuario> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Usuario> usuarios = new Vector<Usuario>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "amazin");

			ps = con.prepareStatement("select id, apellidos, nombre, created, zip, calle, ciudad, email, login"
					+ "  from Usuario u inner join Cliente c on c.id = u.id");
			rs = ps.executeQuery();

			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setNombre(rs.getString("nombre"));
				cliente.setEmail(rs.getString("email"));
				cliente.setApellidos(rs.getString("apellidos"));
				cliente.setCreated(rs.getDate("created"));
				usuarios.add(cliente);
			}

			ps = con.prepareStatement("select id, apellidos, nombre, created,"
					+ " zip, calle, ciudad, email, login"
					+ "  from Usuario u inner join Fotografo c on c.id = u.id");
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografo fotografo = new Fotografo();
				fotografo.setNombre(rs.getString("nombre"));
				fotografo.setEmail(rs.getString("email"));
				fotografo.setApellidos(rs.getString("apellidos"));
				fotografo.setCreated(rs.getDate("created"));
				usuarios.add(fotografo);
			}

			return usuarios;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Usuario persist(Usuario usuario) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("INSERT INTO Usuario ("
					+ "nombre, apellidos, version, calle, zip, ciudad, email,"
					+ " login, password" + ") VALUES (" + "'"
					+ usuario.getNombre()
					+ "',"
					+ "'"
					+ usuario.getApellidos()
					+ "',"
					+ " "
					+ usuario.getVersion()
					+ ","
					+ "'"
					+ usuario.getAddress().getStreet()
					+ "',"
					+ " "
					+ usuario.getAddress().getZipcode()
					+ ","
					+ "'"
					+ usuario.getAddress().getCity()
					+ "',"
					+ "'"
					+ usuario.getEmail()
					+ "',"
					+ "'"
					+ usuario.getLogin()
					+ "'," + "'" + usuario.getPassword() + "'" + ")");

			ps.executeUpdate();

			ps = con.prepareStatement("select max(id) as id" + "  from Usuario");
			rs = ps.executeQuery();

			long max_id = 0;

			if (rs.next()) {
				max_id = rs.getLong("id");
				usuario.setId(max_id);
			}

			if (usuario.getAdmin()) {
				ps = con.prepareStatement("INSERT INTO Fotografo (" + "id"
						+ ") VALUES (" + "'" + max_id + "')");
				ps.executeUpdate();
			} else {
				ps = con.prepareStatement("INSERT INTO Cliente (" + "id"
						+ ") VALUES (" + "'" + max_id + "')");
				ps.executeUpdate();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return usuario;
	}

	@Override
	public Usuario merge(Usuario entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Usuario entity) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void lock(Usuario entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Usuario entity) {
		// TODO Auto-generated method stub

	}

}
