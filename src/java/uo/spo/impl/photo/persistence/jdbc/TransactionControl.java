package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TransactionControl {

	private static ThreadLocal<Connection> connectionsThread = new ThreadLocal<Connection>();

	/**
	 * Parámetros de conexion
	 * */
	private final static String SQL_DRV = "org.hsqldb.jdbcDriver";
	private final static String SQL_URL = "jdbc:hsqldb:hsql://localhost";
	private final static String user = "SA";
	private final static String password = "";

	public static Connection getConnection() throws ClassNotFoundException,
			SQLException {
		Class.forName(SQL_DRV);
		Connection connection = DriverManager.getConnection(SQL_URL, user,
				password);
		
		return connection;
	}

	public static Connection getCurrentConnection() throws SQLException,
			ClassNotFoundException {

		Connection connection = connectionsThread.get();

		if (connection == null) {
			connection = getConnection();
			connectionsThread.set(connection);
		}
		if (connection.isClosed()) {
			connection = getConnection();
			connectionsThread.set(connection);
		}
		return connection;

	}
}
