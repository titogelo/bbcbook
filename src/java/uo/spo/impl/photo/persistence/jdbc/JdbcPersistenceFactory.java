package uo.spo.impl.photo.persistence.jdbc;

import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.impl.photo.business.commands.CommandExecutorFactory;
import uo.spo.photo.persistence.AlbumDao;
import uo.spo.photo.persistence.ClienteDao;
import uo.spo.photo.persistence.EventoDao;
import uo.spo.photo.persistence.FotografiaDao;
import uo.spo.photo.persistence.FotografoDao;
import uo.spo.photo.persistence.PersistenceFactory;
import uo.spo.photo.persistence.UsuarioDao;

/**
 * Devuelve una instancia de un DAO especifica de JPA
 * 
 * If for a particular DAO there is no additional non-CRUD functionality, we use
 * a nested static class to implement the interface in a generic way. This
 * allows clean refactoring later on, should the interface implement business
 * data access methods at some later time. Then, we would externalize the
 * implementation into its own first-level class.
 */
public class JdbcPersistenceFactory implements PersistenceFactory,
		CommandExecutorFactory {

	@Override
	public FotografoDao getFotografoDao() {
		return new FotografoJdbcDao();
	}

	@Override
	public FotografiaDao getFotografiaDao() {
		return new FotografiaJdbcDao();
	}

	@Override
	public ClienteDao getClienteDao() {
		return new ClienteJdbcDao();
	}

	@Override
	public EventoDao getEventoDao() {
		return new EventoJdbcDao();
	}

	@Override
	public AlbumDao getAlbumDao() {
		return new AlbumJdbcDao();
	}

	@Override
	public UsuarioDao getUsuarioDao() {
		return new UsuarioJdbcDao();
	}

	@Override
	public CommandExecutor getCommandExecutor() {
		return new JdbcCommandExecutor();
	}

}
