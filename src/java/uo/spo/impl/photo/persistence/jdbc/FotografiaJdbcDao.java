package uo.spo.impl.photo.persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.FotografiaDao;

public class FotografiaJdbcDao implements FotografiaDao {

	@Override
	public Fotografia findById(Long id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created"
					+ " from Fotografia where id = " + id);
			rs = ps.executeQuery();

			Fotografia fotografia = new Fotografia();

			while (rs.next()) {
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
			}

			return fotografia;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Fotografia> findAll() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		Vector<Fotografia> fotografias = new Vector<Fotografia>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created"
					+ " from Fotografia");
			rs = ps.executeQuery();

			Fotografia fotografia = new Fotografia();

			while (rs.next()) {
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
				fotografias.add(fotografia);
			}

			return fotografias;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Fotografia persist(Fotografia fotografia) throws Exception {
		PreparedStatement ps = null, ps_evento = null, ps_album = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("INSERT INTO Fotografia ("
					+ "nombre, version" + ") VALUES (" + "'"
					+ fotografia.getNombre() + "',0)");
			ps.executeUpdate();

			ps = con.prepareStatement("select max(id) as id"
					+ "  from Fotografia");
			rs = ps.executeQuery();

			@SuppressWarnings("unused")
			long max_id = 0;

			if (rs.next()) {
				fotografia.setId(rs.getLong("id"));
			}

			ps_album = con.prepareStatement("select albums_id  "
					+ "from Fotografia_Album e " + "where fotografias_id = "
					+ fotografia.getId());

			ps_evento = con.prepareStatement("select eventos_id  "
					+ "from Fotografia_Evento e " + "where fotografias_id = "
					+ fotografia.getId());

			Set<Album> albums = fotografia.getAlbums();

			long aux_album = 0;
			boolean esta_album = false;

			long id_album_que_falta = 0;

			/*
			 * Cojo del objeto foto sus albums (A) y tambien cojo los albums de
			 * la foto en BBDD (B). Recorro A y con su id compruebo a ver si
			 * esta en B, si no esta quiere decir que es el album que me falta
			 * por dar de alta en la tabla intermedia. El ultimo paso seria
			 * crear esa relación.
			 */
			// Printer.print("Album");

			if (albums.size() != 0) {
				for (Album album : albums) {
					aux_album = album.getId();

					rs = ps_album.executeQuery();

					while (rs.next() && !esta_album) {

						if (rs.getLong("albums_id") == aux_album) {
							esta_album = true;
						}
					}

					if (!esta_album) {
						id_album_que_falta = aux_album;
					}
					esta_album = false;
				}

				ps = con.prepareStatement("INSERT INTO Fotografia_album ("
						+ "fotografias_id, albums_id" + ") VALUES (" + ""
						+ fotografia.getId() + "," + id_album_que_falta + ")");
				ps.executeUpdate();
			}

			Set<Evento> eventos = fotografia.getEventos();

			long aux_evento = 0;
			boolean esta_evento = false;

			long id_evento_que_falta = 0;

			/*
			 * Cojo del objeto foto sus albums (A) y tambien cojo los albums de
			 * la foto en BBDD (B). Recorro A y con su id compruebo a ver si
			 * esta en B, si no esta quiere decir que es el album que me falta
			 * por dar de alta en la tabla intermedia. El ultimo paso seria
			 * crear esa relación.
			 */
			// Printer.print("Eventos");

			if (eventos.size() != 0) {
				for (Evento evento : eventos) {
					aux_evento = evento.getId();

					rs = ps_evento.executeQuery();

					while (rs.next() && !esta_evento) {

						if (rs.getLong("eventos_id") == aux_evento) {
							esta_evento = true;
						}
					}

					if (!esta_evento) {
						id_evento_que_falta = aux_evento;
					}
					esta_evento = false;
				}

				ps = con.prepareStatement("INSERT INTO Fotografia_evento ("
						+ "fotografias_id, eventos_id" + ") VALUES (" + ""
						+ fotografia.getId() + "," + id_evento_que_falta + ")");
				ps.executeUpdate();

				return fotografia;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return fotografia;
	}

	@Override
	public Fotografia merge(Fotografia fotografia) throws Exception {
		PreparedStatement ps = null, ps_evento = null, ps_album = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("UPDATE Fotografia SET " + "nombre = '"
					+ fotografia.getNombre() + "'" + ", version = " + 0);
			ps.executeUpdate();

			ps_album = con.prepareStatement("select albums_id  "
					+ "from Fotografia_Album e " + "where fotografias_id = "
					+ fotografia.getId());

			ps_evento = con.prepareStatement("select eventos_id  "
					+ "from Fotografia_Evento e " + "where fotografias_id = "
					+ fotografia.getId());

			Set<Album> albums = fotografia.getAlbums();

			long aux_album = 0;
			boolean esta_album = false;

			long id_album_que_falta = 0;

			/*
			 * Cojo del objeto foto sus albums (A) y tambien cojo los albums de
			 * la foto en BBDD (B). Recorro A y con su id compruebo a ver si
			 * esta en B, si no esta quiere decir que es el album que me falta
			 * por dar de alta en la tabla intermedia. El ultimo paso seria
			 * crear esa relación.
			 */
			// Printer.print("Album");

			if (albums.size() != 0) {
				for (Album album : albums) {
					aux_album = album.getId();

					rs = ps_album.executeQuery();

					while (rs.next() && !esta_album) {

						if (rs.getLong("albums_id") == aux_album) {
							esta_album = true;
						}
					}

					if (!esta_album) {
						id_album_que_falta = aux_album;
					}
					esta_album = false;
				}

				ps = con.prepareStatement("INSERT INTO Fotografia_album ("
						+ "fotografias_id, albums_id" + ") VALUES (" + ""
						+ fotografia.getId() + "," + id_album_que_falta + ")");
				ps.executeUpdate();
			}

			Set<Evento> eventos = fotografia.getEventos();

			long aux_evento = 0;
			boolean esta_evento = false;

			long id_evento_que_falta = 0;

			/*
			 * Cojo del objeto foto sus albums (A) y tambien cojo los albums de
			 * la foto en BBDD (B). Recorro A y con su id compruebo a ver si
			 * esta en B, si no esta quiere decir que es el album que me falta
			 * por dar de alta en la tabla intermedia. El ultimo paso seria
			 * crear esa relación.
			 */
			// Printer.print("Eventos");

			if (eventos.size() != 0) {
				for (Evento evento : eventos) {
					aux_evento = evento.getId();

					rs = ps_evento.executeQuery();

					while (rs.next() && !esta_evento) {

						if (rs.getLong("eventos_id") == aux_evento) {
							esta_evento = true;
						}
					}

					if (!esta_evento) {
						id_evento_que_falta = aux_evento;
					}
					esta_evento = false;
				}

				ps = con.prepareStatement("INSERT INTO Fotografia_evento ("
						+ "fotografias_id, eventos_id" + ") VALUES (" + ""
						+ fotografia.getId() + "," + id_evento_que_falta + ")");
				ps.executeUpdate();

				return fotografia;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return fotografia;
	}

	@Override
	public void remove(Fotografia entity) throws Exception {
		PreparedStatement ps = null;

		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("delete from Fotografia " + "where id = "
					+ entity.getId());
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void lock(Fotografia entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh(Fotografia entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Fotografia> findFotografiaByEvento(Long eventoId)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Fotografia> fotografias = new ArrayList<Fotografia>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created  "
					+ "from Fotografia_Evento e inner join Fotografia f on "
					+ "f.id = e.fotografias_id " + "where eventos_id = "
					+ eventoId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografia fotografia = new Fotografia();
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
				fotografias.add(fotografia);
			}

			return fotografias;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<Fotografia> findFotografiaByAlbum(Long albumId)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Fotografia> fotografias = new ArrayList<Fotografia>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "SA", "");

			ps = con.prepareStatement("select id, nombre, created  "
					+ "from Fotografia_Album e inner join Fotografia f on "
					+ "f.id = e.fotografias_id " + "where albums_id = "
					+ albumId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Fotografia fotografia = new Fotografia();
				fotografia.setId(rs.getLong("id"));
				fotografia.setNombre(rs.getString("nombre"));
				fotografia.setCreated(rs.getDate("created"));
				fotografias.add(fotografia);
			}

			return fotografias;

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
	}

}
