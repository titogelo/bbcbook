package uo.spo.impl.photo.persistence.jpa;

import java.util.List;

import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.FotografiaDao;

/**
 * Implementacion JPA especifica de FotografiaDAO.
 */
public class FotografiaJpaDao extends GenericJpaDao<Fotografia> implements
		FotografiaDao {

	@Override
	public List<Fotografia> findFotografiaByEvento(Long eventoId) {
		if (eventoId == null) {
			throw new RuntimeException("El evento debe existir.");
		}
		return findByNamedQuery("findFotografiasByEvento", eventoId);
	}

	@Override
	public List<Fotografia> findFotografiaByAlbum(Long albumId) {
		if (albumId == null) {
			throw new RuntimeException("El album debe existir.");
		}
		return findByNamedQuery("findFotografiasByAlbum", albumId);
	}

}
