package uo.spo.impl.photo.persistence.jpa;

import uo.spo.photo.model.Usuario;
import uo.spo.photo.persistence.UsuarioDao;

/**
 * Implementacion JPA especifica de UsarioDAO.
 */
public class UsuarioJpaDao extends GenericJpaDao<Usuario> implements UsuarioDao {

}
