package uo.spo.impl.photo.persistence.jpa;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;

import uo.spo.photo.persistence.GenericDao;

/**
 * Implementacion genérica de las operaciones CRUD usando el API de JPA
 */
public abstract class GenericJpaDao<T> implements GenericDao<T> {

	private Class<T> persistentClass;
	private EntityManager em;

	public GenericJpaDao() {
		persistentClass = inferCurrentTypeForT();
	}

	@Override
	public T findById(Long id) {
		T entity = (T) getEntityManager().find(persistentClass, id);
		return entity;
	}

	@Override
	public List<T> findAll() {
		String queryString = "select x from " + persistentClass.getSimpleName()
				+ " x";
		return findByQuery(queryString);
	}

	@Override
	public T persist(T entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public void remove(T entity) {
		getEntityManager().remove(entity);
	}

	@Override
	public T merge(T entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public void lock(T entity) {
		getEntityManager().lock(entity, LockModeType.WRITE);
	}

	@Override
	public void refresh(T entity) {
		getEntityManager().flush(); // synchronize with Data Base...
		getEntityManager().refresh(entity); // ... then reload it
	}

	@SuppressWarnings("unchecked")
	public List<T> findByQuery(String queryString, Object... values) {
		Query qry = getEntityManager().createQuery(queryString);
		bindParameters(qry, values);
		return (List<T>) qry.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByNamedQuery(String queryName, Object... values) {
		Query qry = getEntityManager().createNamedQuery(queryName);
		bindParameters(qry, values);

		return (List<T>) qry.getResultList();
	}

	@SuppressWarnings("unchecked")
	public T findUniqueByNamedQuery(String queryName, Object... values) {
		Query qry = getEntityManager().createNamedQuery(queryName);
		bindParameters(qry, values);
		return (T) qry.getSingleResult();
	}

	public Long countByNamedQuery(String queryName, Object... values) {
		Query qry = getEntityManager().createNamedQuery(queryName);
		bindParameters(qry, values);
		return (Long) qry.getSingleResult();
	}

	protected EntityManager getEntityManager() {
		if (em == null) {
			em = PersistenceUtil.getCurrentEntityManager();
		}
		return em;
	}

	private void bindParameters(Query qry, Object... values) {
		int i = 1;
		for (Object arg : values) {
			qry.setParameter(i++, arg);
		}
	}

	/**
	 * Returns the runtime type of T in GenericJpaDAO<T>
	 */
	@SuppressWarnings("unchecked")
	private Class<T> inferCurrentTypeForT() {
		Type parentClassType = getClass().getGenericSuperclass();
		ParameterizedType parentAsParameterized = (ParameterizedType) parentClassType;
		return (Class<T>) parentAsParameterized.getActualTypeArguments()[0];
	}

}
