package uo.spo.impl.photo.persistence.jpa;

import java.util.List;

import uo.spo.photo.model.Evento;
import uo.spo.photo.persistence.EventoDao;

/**
 * Implementacion JPA especifica de EventoDAO.
 */
public class EventoJpaDao extends GenericJpaDao<Evento> implements EventoDao {

	@Override
	public List<Evento> getEventosByFotografo(Long fotografoId) {
		if (fotografoId == null) {
			throw new RuntimeException("El fotografo debe existir.");
		}
		return findByNamedQuery("findEventosByFotografo", fotografoId);
	}

	@Override
	public List<Evento> findEventosByCliente(Long clienteId) {
		if (clienteId == null) {
			throw new RuntimeException("El cliente debe existir.");
		}
		return findByNamedQuery("findEventosByCliente", clienteId);
	}
}
