package uo.spo.impl.photo.persistence.jpa;

import uo.spo.photo.model.Fotografo;
import uo.spo.photo.persistence.FotografoDao;

/**
 * Implementacion JPA especifica de FotografoDAO.
 */
public class FotografoJpaDao extends GenericJpaDao<Fotografo> implements
		FotografoDao {

	public Long countNumberOfFotografiasInAlbum(Long albumId) {
		if (albumId == null) {
			return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum");
		}
		return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum",
				albumId);
	}

	@Override
	public Fotografo validateLogin(Fotografo fotografo) {
		Fotografo u = (Fotografo) getEntityManager()
				.createNamedQuery("findFotografoByLogin")
				.setParameter("pass", fotografo.getPassword())
				.setParameter("username", fotografo.getLogin())
				.getSingleResult();
		return u;
	}

}
