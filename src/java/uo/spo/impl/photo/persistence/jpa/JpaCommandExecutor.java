package uo.spo.impl.photo.persistence.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uo.spo.impl.photo.business.commands.Command;
import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.infrastructure.Factories;

/**
 * CommandoExecutor para JPA, si necesitas cambiar alguna capa de persistencia,
 * es necesario cambiar cosas aqui. Esta clase se encarga de: - Gestionar el
 * EntityManager para cada hilo - Manejar la transacción.
 */
public class JpaCommandExecutor implements CommandExecutor {
	private static Logger log = LoggerFactory
			.getLogger(JpaCommandExecutor.class);

	@Override
	public Object execute(Command c) throws Exception {
		Object res = null;

		// Note getCurrentEntityManager() instead of getEntityManager()
		EntityManager em = PersistenceUtil.getCurrentEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		try {
			c.setDAOFactory(Factories.persistence);

			res = c.execute();

			tx.commit();
		} catch (BusinessException bex) {
			rollback(c, tx, bex);
			throw bex;
		} catch (RuntimeException rex) {
			rollback(c, tx, rex);
			throw rex;
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return res;
	}

	private void rollback(Command c, EntityTransaction tx, Exception ex) {
		try {
			if (tx.isActive()) {
				tx.rollback();
			}
			log.debug("rollback by exception in " + c.getClass().getName(), ex);
		} catch (RuntimeException rbEx) {
			log.error("Couldn�t roll back transaction in "
					+ c.getClass().getName(), rbEx);
		}
	}
}
