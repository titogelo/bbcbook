package uo.spo.impl.photo.persistence.jpa;

import java.util.List;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.AlbumFilterPhoto;
import uo.spo.photo.persistence.AlbumDao;

/**
 * Implementacion JPA especifica de AlbumDAO.
 */

public class AlbumJpaDao extends GenericJpaDao<Album> implements AlbumDao {

	public Long countNumberOfFotografiasInAlbum(Long albumId) {
		if (albumId == null) {
			return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum");
		}
		return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum",
				albumId);
	}

	@Override
	public Long fotografiasMediaPorAlbum() {
		return countByNamedQuery("mediaFotografiasPorAlbum");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlbumFilterPhoto> albumsByNumFotografias(Long num_fotografias) {
		List<AlbumFilterPhoto> qry = (List<AlbumFilterPhoto>) getEntityManager()
				.createQuery("filtroAlbumsByFotografias",
						AlbumFilterPhoto.class).setParameter("numero", 2);
		return qry;
	}

	@Override
	public List<Album> findAlbumByCliente(Long clienteId) throws Exception {
		if (clienteId == null) {
			throw new RuntimeException("El cliente debe existir.");
		}
		return findByNamedQuery("findAlbumByCliente", clienteId);
	}

	@Override
	public void updateEventAndAlbum(Long albumId) throws Exception {
		
	}

}
