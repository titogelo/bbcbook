package uo.spo.impl.photo.persistence.jpa;

import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.impl.photo.business.commands.CommandExecutorFactory;
import uo.spo.photo.persistence.AlbumDao;
import uo.spo.photo.persistence.ClienteDao;
import uo.spo.photo.persistence.EventoDao;
import uo.spo.photo.persistence.FotografiaDao;
import uo.spo.photo.persistence.FotografoDao;
import uo.spo.photo.persistence.PersistenceFactory;
import uo.spo.photo.persistence.UsuarioDao;

/**
 * Devuelve una instancia de un DAO especifica de JPA
 * 
 * If for a particular DAO there is no additional non-CRUD functionality, we use
 * a nested static class to implement the interface in a generic way. This
 * allows clean refactoring later on, should the interface implement business
 * data access methods at some later time. Then, we would externalize the
 * implementation into its own first-level class.
 */
public class JpaPersistenceFactory implements PersistenceFactory,
		CommandExecutorFactory {

	@Override
	public CommandExecutor getCommandExecutor() {
		return new JpaCommandExecutor();
	}

	@Override
	public FotografoDao getFotografoDao() {
		return new FotografoJpaDao();
	}

	@Override
	public FotografiaDao getFotografiaDao() {
		return new FotografiaJpaDao();
	}

	@Override
	public ClienteDao getClienteDao() {
		return new ClienteJpaDao();
	}

	@Override
	public EventoDao getEventoDao() {
		return new EventoJpaDao();
	}

	@Override
	public AlbumDao getAlbumDao() {
		return new AlbumJpaDao();
	}

	@Override
	public UsuarioDao getUsuarioDao() {
		return new UsuarioJpaDao();
	}

}
