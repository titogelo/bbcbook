package uo.spo.impl.photo.persistence.jpa;

import uo.spo.photo.model.Cliente;
import uo.spo.photo.persistence.ClienteDao;

/**
 * Implementacion JPA especifica de ClienteDAO.
 */
public class ClienteJpaDao extends GenericJpaDao<Cliente> implements ClienteDao {

	public Long countNumberOfFotografiasInAlbum(Long albumId) {
		if (albumId == null) {
			return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum");
		}
		return (Long) countByNamedQuery("countNumberOfFotografiasInAlbum",
				albumId);
	}

	@Override
	public Cliente validateLogin(Cliente cliente) {
		Cliente u = (Cliente) getEntityManager()
				.createNamedQuery("findClienteByLogin")
				.setParameter("pass", cliente.getPassword())
				.setParameter("username", cliente.getLogin()).getSingleResult();
		return u;
	}

}
