package uo.spo.impl.photo.business;

import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.impl.photo.business.commands.fotografo.ClienteLoginCommand;
import uo.spo.impl.photo.business.commands.fotografo.FotografoLoginCommand;
import uo.spo.impl.photo.business.commands.fotografo.UserRegisterNewCommand;
import uo.spo.photo.business.UsuarioRegistradoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Usuario;

public class UsuarioRegistradoServicesImpl implements UsuarioRegistradoServices {
	private CommandExecutor executor = Factories.executor.getCommandExecutor();

	@Override
	public void userRegisterNew(Usuario user) throws Exception {
		executor.execute(new UserRegisterNewCommand(user));
	}

	@Override
	public Long fotografoLogin(String username, String pass) throws Exception {
		return (Long) executor
				.execute(new FotografoLoginCommand(username, pass));
	}

	@Override
	public Long clienteLogin(String username, String pass) throws Exception {
		return (Long) executor.execute(new ClienteLoginCommand(username, pass));
	}
}
