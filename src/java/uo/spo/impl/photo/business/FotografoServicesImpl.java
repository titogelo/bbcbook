package uo.spo.impl.photo.business;

import java.util.List;

import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.impl.photo.business.commands.fotografo.EventoCreateCommand;
import uo.spo.impl.photo.business.commands.fotografo.EventoDeleteCommand;
import uo.spo.impl.photo.business.commands.fotografo.EventoUpdateCommand;
import uo.spo.impl.photo.business.commands.fotografo.FotografiaCreateCommand;
import uo.spo.impl.photo.business.commands.fotografo.FotografoUpdateCommand;
import uo.spo.impl.photo.business.commands.fotografo.GetEventoFotografoCommand;
import uo.spo.impl.photo.business.commands.fotografo.GetFotografiasEventoCommand;
import uo.spo.impl.photo.business.commands.util.DirectDaoCommand;
import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.model.Usuario;

public class FotografoServicesImpl implements FotografoServices {
	private CommandExecutor executor = Factories.executor.getCommandExecutor();

	@Override
	public void eventoUpdate(Evento evento) throws Exception {
		executor.execute(new EventoUpdateCommand(evento));
	}

	@Override
	public void fotografoUpdate(Fotografo fotografo) throws Exception {
		executor.execute(new FotografoUpdateCommand(fotografo));
	}

	@Override
	public Evento eventoCreate(Evento evento) throws Exception {
		return (Evento) executor.execute(new EventoCreateCommand(evento));

	}

	@Override
	public void eventoDelete(Evento evento) throws Exception {
		executor.execute(new EventoDeleteCommand(evento));
	}

	@Override
	public void fotografiaCreate(Fotografia fotografia) throws Exception {
		executor.execute(new FotografiaCreateCommand(fotografia));
	}

	@Override
	public Evento getFotosByEvento(Long eventoId) throws Exception {
		return (Evento) executor.execute(new GetFotografiasEventoCommand(
				eventoId));
	}

	@Override
	public Fotografo getEventoByFotografo(Long fotografoId) throws Exception {
		return (Fotografo) executor.execute(new GetEventoFotografoCommand(
				fotografoId));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> getAllEventos() throws Exception {
		return (List<Evento>) executor.execute(new DirectDaoCommand(
				"EventoDao", "findAll"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getAllFotografos() throws Exception {
		return (List<Usuario>) executor.execute(new DirectDaoCommand(
				"FotografoDao", "findAll"));
	}

	@Override
	public Evento getEventoById(Long eventoId) throws Exception {
		return (Evento) executor.execute(new DirectDaoCommand("EventoDao",
				"findById", eventoId));
	}

	@Override
	public Fotografo getFotografoById(Long fotografoId) throws Exception {
		return (Fotografo) executor.execute(new DirectDaoCommand(
				"FotografoDao", "findById", fotografoId));
	}

	@Override
	public Cliente getClienteById(Long clienteId) throws Exception {
		return (Cliente) executor.execute(new DirectDaoCommand("ClienteDao",
				"findById", clienteId));
	}
}
