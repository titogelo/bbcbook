package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Evento;

public class GetFotografiasEventoCommand extends AbstractCommand {
	private Long eventoId;

	public GetFotografiasEventoCommand(Long eventoId) {
		this.eventoId = eventoId;
	}

	@Override
	public Object execute() throws Exception {
		Evento evento = daoFactory.getEventoDao().findById(eventoId);

		if (evento == null) {
			throw new BusinessException("No existe ese album ID");
		}

		evento.getFotografias().size();

		return evento;
	}

}