package uo.spo.impl.photo.business.commands;

import uo.spo.photo.business.BusinessException;
import uo.spo.photo.persistence.PersistenceFactory;

public interface Command {

	Object execute() throws BusinessException, Exception;

	/**
	 * Recibe la factoria a emplear para obtener los DAOs. Se la establece el
	 * CommandExecutor antes de invocar al método execute()
	 * 
	 * @param factory
	 */
	void setDAOFactory(PersistenceFactory factory);
}
