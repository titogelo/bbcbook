package uo.spo.impl.photo.business.commands.cliente;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Album;
import uo.spo.photo.persistence.AlbumDao;

public class AlbumCreateCommand extends AbstractCommand {
	private Album album;

	public AlbumCreateCommand(Album album) {
		super();
		this.album = album;
	}

	@Override
	public Album execute() throws Exception {

		AlbumDao dao = daoFactory.getAlbumDao();

		if (album == null) {
			throw new BusinessException("No fue posible persistir el album.");
		}
		dao.persist(album);

		return album;
	}
}
