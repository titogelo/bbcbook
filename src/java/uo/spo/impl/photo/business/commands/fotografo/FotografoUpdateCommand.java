package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.persistence.FotografoDao;

public class FotografoUpdateCommand extends AbstractCommand {
	private Fotografo fotografo;

	public FotografoUpdateCommand(Fotografo fotografo) {
		super();
		this.fotografo = fotografo;
	}

	@Override
	public Object execute() throws Exception {

		FotografoDao dao = daoFactory.getFotografoDao();

		if (fotografo == null) {
			throw new BusinessException(
					"No fue posible hacer upadte del fotografo.");
		}

		dao.merge(fotografo);

		return null;
	}
}
