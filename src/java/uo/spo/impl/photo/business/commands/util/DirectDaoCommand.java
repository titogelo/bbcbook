package uo.spo.impl.photo.business.commands.util;

import java.lang.reflect.Method;

import uo.spo.photo.business.BusinessException;

public class DirectDaoCommand extends AbstractCommand {
	private String daoType;
	private String daoMethod;
	private Object[] args;

	@Override
	public Object execute() throws BusinessException {
		Object daoObject = getDaoInstance(daoType);
		Method method = ReflectionUtil.getMethodByName(daoObject.getClass(),
				daoMethod, args);

		return ReflectionUtil.invokeMethodOnObject(daoObject, method, args);
	}

	public DirectDaoCommand(String daoType, String daoMethod, Object... args) {
		this.daoType = daoType;
		this.daoMethod = daoMethod;
		this.args = args;
	}

	/**
	 * Devuelve un objeto del tipo especificado por el parámetro pidíendoselo a
	 * la factoria de DAOs
	 * 
	 * @param daoType
	 * @return
	 */
	private Object getDaoInstance(String daoType) {
		String fullMethodName = "get" + capitalize(daoType);
		Method factoryMethod = ReflectionUtil.getMethodByName(
				daoFactory.getClass(), fullMethodName);
		return ReflectionUtil.invokeMethodOnObject(daoFactory, factoryMethod);
	}

	/**
	 * Devuelve un string (otro) con el primer carácter puesto en mayúsculas
	 * 
	 * @param s
	 * @return
	 */
	private String capitalize(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

}
