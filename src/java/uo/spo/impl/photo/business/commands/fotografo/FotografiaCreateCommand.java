package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.FotografiaDao;

public class FotografiaCreateCommand extends AbstractCommand {
	private Fotografia fotografia;

	public FotografiaCreateCommand(Fotografia fotografia) {
		super();
		this.fotografia = fotografia;
	}

	@Override
	public Object execute() throws Exception {

		FotografiaDao dao = daoFactory.getFotografiaDao();

		if (fotografia == null) {
			throw new BusinessException(
					"No fue posible persistir la fotografía.");
		}
		dao.persist(fotografia);

		return null;
	}
}
