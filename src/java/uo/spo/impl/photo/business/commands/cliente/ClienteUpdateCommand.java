package uo.spo.impl.photo.business.commands.cliente;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.persistence.ClienteDao;

public class ClienteUpdateCommand extends AbstractCommand {
	private Cliente cliente;

	public ClienteUpdateCommand(Cliente cliente) {
		super();
		this.cliente = cliente;
	}

	@Override
	public Object execute() throws Exception {

		ClienteDao dao = daoFactory.getClienteDao();

		if (cliente == null) {
			throw new BusinessException(
					"No fue posible hacer upadte del cliente.");
		}
		dao.merge(cliente);

		return null;
	}
}
