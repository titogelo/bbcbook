package uo.spo.impl.photo.business.commands;

public interface CommandExecutorFactory {

	CommandExecutor getCommandExecutor();

}
