package uo.spo.impl.photo.business.commands.cliente;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Album;

public class GetFotografiasAlbumCommand extends AbstractCommand {
	private Long albumId;

	public GetFotografiasAlbumCommand(Long albumId) {
		this.albumId = albumId;
	}

	@Override
	public Object execute() throws Exception {
		Album album = daoFactory.getAlbumDao().findById(albumId);

		if (album == null) {
			throw new BusinessException("No existe ese album ID");
		}

		album.getFotografia().size();

		return album;
	}

}