package uo.spo.impl.photo.business.commands.cliente;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Cliente;

public class GetEventoClienteCommand extends AbstractCommand {
	private Long clienteId;

	public GetEventoClienteCommand(Long clienteId) {
		this.clienteId = clienteId;
	}

	@Override
	public Object execute() throws Exception {
		Cliente cliente = daoFactory.getClienteDao().findById(clienteId);

		if (cliente == null) {
			throw new BusinessException("No existe ese cliente ID");
		}

		cliente.getEventos().size();

		return cliente;
	}

}