package uo.spo.impl.photo.business.commands.fotografo;

import javax.persistence.EntityExistsException;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Usuario;

public class UserRegisterNewCommand extends AbstractCommand {
	private Usuario user;

	public UserRegisterNewCommand(Usuario user) {
		this.user = user;
	}

	@Override
	public Object execute() throws Exception {

		try {
			daoFactory.getUsuarioDao().persist(user);
		} catch (EntityExistsException eee) {
			throw new BusinessException("Ese login ya esta registrado");
		}

		return null;
	}

}
