package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Evento;
import uo.spo.photo.persistence.EventoDao;

public class EventoUpdateCommand extends AbstractCommand {
	private Evento evento;

	public EventoUpdateCommand(Evento evento) {
		super();
		this.evento = evento;
	}

	@Override
	public Object execute() throws Exception {

		EventoDao dao = daoFactory.getEventoDao();

		if (evento == null) {
			throw new BusinessException(
					"No fue posible hacer update del evento.");
		}
		dao.merge(evento);

		return null;
	}
}
