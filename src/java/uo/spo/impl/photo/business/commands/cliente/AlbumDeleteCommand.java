package uo.spo.impl.photo.business.commands.cliente;

import java.util.Set;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.AlbumDao;
import uo.spo.photo.persistence.FotografiaDao;

public class AlbumDeleteCommand extends AbstractCommand {
	private Album album;

	public AlbumDeleteCommand(Album album) {
		super();
		this.album = album;
	}

	@Override
	public Object execute() throws Exception {

		AlbumDao dao = daoFactory.getAlbumDao();
		FotografiaDao daoFoto = daoFactory.getFotografiaDao();

		if (album == null) {
			throw new BusinessException(
					"No fue posible hacer remove del album.");
		}
		Album album1 = dao.findById(album.getId());

		if (album1.getFotografia().size() == 0) {
			dao.remove(album1);
		} else {
			Set<Fotografia> lista = album1.getFotografia();

			for (Fotografia fotografia : lista) {
				daoFoto.remove(fotografia);
			}
			dao.remove(album1);
		}

		return null;
	}
}
