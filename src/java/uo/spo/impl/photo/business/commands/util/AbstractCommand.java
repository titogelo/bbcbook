package uo.spo.impl.photo.business.commands.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uo.spo.impl.photo.business.commands.Command;
import uo.spo.photo.persistence.PersistenceFactory;

/**
 * Implmenta Command y añade log particularizado para la clase heredera y
 * asignación de la DAOFactory
 * 
 * Deberán heredar de esta clase todos los Commands que necesitan acceso a datos
 * persistentes
 * 
 * @author Angel
 * 
 */
public abstract class AbstractCommand implements Command {
	protected Logger log;
	protected PersistenceFactory daoFactory;

	protected AbstractCommand() {
		super();
		log = LoggerFactory.getLogger(this.getClass());
	}

	@Override
	public void setDAOFactory(PersistenceFactory factory) {
		daoFactory = factory;
	}
}
