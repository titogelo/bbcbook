package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Fotografo;

public class GetEventoFotografoCommand extends AbstractCommand {
	private Long fotografoId;

	public GetEventoFotografoCommand(Long fotografoId) {
		this.fotografoId = fotografoId;
	}

	@Override
	public Object execute() throws Exception {
		Fotografo fotografo = daoFactory.getFotografoDao()
				.findById(fotografoId);

		if (fotografo == null) {
			throw new BusinessException("No existe ese fotografo ID");
		}

		fotografo.getEventos().size();

		return fotografo;
	}

}