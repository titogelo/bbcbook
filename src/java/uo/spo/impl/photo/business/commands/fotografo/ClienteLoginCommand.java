package uo.spo.impl.photo.business.commands.fotografo;

import javax.persistence.NoResultException;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Cliente;

public class ClienteLoginCommand extends AbstractCommand {
	private String userName;
	private String pass;

	public ClienteLoginCommand(String userName, String pass) {
		super();
		this.userName = userName;
		this.pass = pass;
	}

	@Override
	public Object execute() throws Exception {
		Cliente cliente = new Cliente("first Name", "last Name", userName,
				pass, "email");
		try {
			cliente = daoFactory.getClienteDao().validateLogin(cliente);
		} catch (NoResultException nre) {
			throw new BusinessException("User login incorrect", nre);
		}
		return cliente.getId();
	}
}
