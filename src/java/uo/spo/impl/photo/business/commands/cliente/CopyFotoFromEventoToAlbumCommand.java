package uo.spo.impl.photo.business.commands.cliente;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.FotografiaDao;

public class CopyFotoFromEventoToAlbumCommand extends AbstractCommand {
	private Album album;
	@SuppressWarnings("unused")
	private Long eventoId;
	private Long fotografiaId;

	public CopyFotoFromEventoToAlbumCommand(Long eventoId, Long fotoId,
			Album album) {
		super();
		this.album = album;
		this.eventoId = eventoId;
		this.fotografiaId = fotoId;
	}

	@Override
	public Object execute() throws Exception {

		FotografiaDao dao = daoFactory.getFotografiaDao();

		Fotografia fotografia = dao.findById(fotografiaId);

		fotografia.addAlbum(album);
		album.addFotografia(fotografia);

		if ((album == null) | (fotografia == null)) {
			throw new BusinessException("No fue posible copiar la fotografia.");
		}

		/*
		 * Este no hace falta para jpa pero para jdbc si, porque hay que añadir
		 * al metodo merge un trozo de codigo que me busque los albums en los
		 * que ya esta la fotografia y que me diga en cuales me faltan y
		 * añadirlo.
		 */
		//dao.merge(fotografia);

		return null;
	}
}
