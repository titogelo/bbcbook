package uo.spo.impl.photo.business.commands.fotografo;

import java.util.List;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.persistence.EventoDao;
import uo.spo.photo.persistence.FotografiaDao;

public class EventoDeleteCommand extends AbstractCommand {
	private Evento evento;

	public EventoDeleteCommand(Evento evento) {
		super();
		this.evento = evento;
	}

	@Override
	public Object execute() throws Exception {

		EventoDao dao = daoFactory.getEventoDao();
		FotografiaDao daoFoto = daoFactory.getFotografiaDao();

		if (evento == null) {
			throw new BusinessException(
					"No fue posible hacer remove del evento.");
		}
		Evento evento1 = dao.findById(evento.getId());

		if (evento1.getFotografias().size() == 0) {
			dao.remove(evento1);
		} else {
			List<Fotografia> lista = (List<Fotografia>) evento1
					.getFotografias();

			for (Fotografia fotografia : lista) {
				daoFoto.remove(fotografia);
			}
			dao.remove(evento1);
		}

		return null;
	}
}
