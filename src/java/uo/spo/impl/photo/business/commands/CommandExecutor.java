package uo.spo.impl.photo.business.commands;

import uo.spo.photo.business.BusinessException;

public interface CommandExecutor {

	Object execute(Command c) throws BusinessException, Exception;

}