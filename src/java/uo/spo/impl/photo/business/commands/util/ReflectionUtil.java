package uo.spo.impl.photo.business.commands.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtil {

	public static Method getMethodByName(Class<?> clazz, String methodName,
			Object... args) {
		Method method = null;
		try {

			Class<?>[] argsTypes = getTypesArrayOfArgs(args);
			method = clazz.getMethod(methodName, argsTypes);

		} catch (SecurityException e) {
			handle(e);
		} catch (NoSuchMethodException e) {
			handle(e);
		}
		return method;
	}

	private static Class<?>[] getTypesArrayOfArgs(Object[] args) {
		if (args == null)
			return null;

		int size = args.length;
		Class<?>[] argsTypes = new Class<?>[size];
		for (int i = 0; i < size; i++) {
			argsTypes[i] = args[i].getClass();
		}
		return argsTypes;
	}

	public static Object invokeMethodOnObject(Object object, Method method,
			Object... args) {
		try {

			return method.invoke(object, args);

		} catch (IllegalArgumentException e) {
			handle(e);
		} catch (IllegalAccessException e) {
			handle(e);
		} catch (InvocationTargetException e) {
			handle(e);
		}
		return null;
	}

	private static void handle(Exception e) {
		throw new RuntimeException(e);
	}

}
