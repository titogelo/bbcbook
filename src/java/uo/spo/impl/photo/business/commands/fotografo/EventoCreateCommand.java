package uo.spo.impl.photo.business.commands.fotografo;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Evento;
import uo.spo.photo.persistence.EventoDao;

public class EventoCreateCommand extends AbstractCommand {
	private Evento evento;

	public EventoCreateCommand(Evento evento) {
		super();
		this.evento = evento;
	}

	@Override
	public Evento execute() throws Exception {

		EventoDao dao = daoFactory.getEventoDao();

		if (evento == null) {
			throw new BusinessException("No fue posible persistir el evento.");
		}
		dao.persist(evento);

		return evento;
	}
}
