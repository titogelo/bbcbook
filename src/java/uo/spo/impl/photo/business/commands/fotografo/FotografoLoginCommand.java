package uo.spo.impl.photo.business.commands.fotografo;

import javax.persistence.NoResultException;

import uo.spo.impl.photo.business.commands.util.AbstractCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.model.Fotografo;

public class FotografoLoginCommand extends AbstractCommand {
	private String userName;
	private String pass;

	public FotografoLoginCommand(String userName, String pass) {
		super();
		this.userName = userName;
		this.pass = pass;
	}

	@Override
	public Object execute() throws Exception {
		Fotografo fotografo = new Fotografo("first Name", "last Name",
				userName, pass, "email");
		try {
			fotografo = daoFactory.getFotografoDao().validateLogin(fotografo);
		} catch (NoResultException nre) {
			throw new BusinessException("User login incorrect", nre);
		}

		return fotografo.getId();
	}
}
