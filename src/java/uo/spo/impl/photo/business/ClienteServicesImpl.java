package uo.spo.impl.photo.business;

import java.util.List;

import uo.spo.impl.photo.business.commands.CommandExecutor;
import uo.spo.impl.photo.business.commands.cliente.AlbumCreateCommand;
import uo.spo.impl.photo.business.commands.cliente.AlbumDeleteCommand;
import uo.spo.impl.photo.business.commands.cliente.ClienteUpdateCommand;
import uo.spo.impl.photo.business.commands.cliente.CopyFotoFromEventoToAlbumCommand;
import uo.spo.impl.photo.business.commands.cliente.GetAlbumClienteCommand;
import uo.spo.impl.photo.business.commands.cliente.GetEventoClienteCommand;
import uo.spo.impl.photo.business.commands.cliente.GetFotografiasAlbumCommand;
import uo.spo.impl.photo.business.commands.fotografo.GetFotografiasEventoCommand;
import uo.spo.impl.photo.business.commands.util.DirectDaoCommand;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.AlbumFilterPhoto;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Usuario;

public class ClienteServicesImpl implements ClienteServices {
	private CommandExecutor executor = Factories.executor.getCommandExecutor();

	/******************** Cliente ********************************************/
	/*
	 * Album
	 */
	@Override
	public Cliente getAlbumByCliente(Long clienteId) throws Exception {
		return (Cliente) executor
				.execute(new GetAlbumClienteCommand(clienteId));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Album> getAllAlbums() throws Exception {
		return (List<Album>) executor.execute(new DirectDaoCommand("AlbumDao",
				"findAll"));
	}

	@Override
	public void albumUpdate(Album album) throws Exception {
		executor.execute(new DirectDaoCommand("AlbumDao", "merge", album));
	}

	@Override
	public Album albumCreate(Album album) throws Exception {
		return (Album) executor.execute(new AlbumCreateCommand(album));
	}

	@Override
	public void albumDelete(Album Album) throws Exception {
		executor.execute(new AlbumDeleteCommand(Album));
	}

	@Override
	public Album getFotosByAlbum(Long albumId) throws Exception {
		return (Album) executor
				.execute(new GetFotografiasAlbumCommand(albumId));
	}

	/*
	 * Evento
	 */
	@Override
	public Cliente getEventoByCliente(Long clienteId) throws Exception {
		return (Cliente) executor
				.execute(new GetEventoClienteCommand(clienteId));
	}

	/*
	 * Cliente
	 */
	@Override
	public Cliente getClienteById(Long clienteId) throws Exception {
		return (Cliente) executor.execute(new DirectDaoCommand("ClienteDao",
				"findById", clienteId));
	}

	@Override
	public void clienteUpdate(Cliente cliente) throws Exception {
		executor.execute(new ClienteUpdateCommand(cliente));
	}

	@Override
	public Evento getFotosByEvento(Long eventoId) throws Exception {
		return (Evento) executor.execute(new GetFotografiasEventoCommand(
				eventoId));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getAllClientes() throws Exception {
		return (List<Usuario>) executor.execute(new DirectDaoCommand(
				"ClienteDao", "findAll"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> getAllEventos() throws Exception {
		return (List<Evento>) executor.execute(new DirectDaoCommand(
				"EventoDao", "findAll"));
	}

	@Override
	public Album getAlbumById(Long albumId) throws Exception {
		return (Album) executor.execute(new DirectDaoCommand("AlbumDao",
				"findById", albumId));
	}

	@Override
	public Evento getEventoById(Long eventoId) throws Exception {
		return (Evento) executor.execute(new DirectDaoCommand("EventoDao",
				"findById", eventoId));
	}

	@Override
	public Fotografia getFotografiaById(Long fotografiaId) throws Exception {
		return (Fotografia) executor.execute(new DirectDaoCommand(
				"FotografiaDao", "findById", fotografiaId));

	}

	@Override
	public int getNumFotografiasAlbum(Long albumId) throws Exception {
		return (Integer) executor.execute(new DirectDaoCommand("AlbumDao",
				"countNumberOfFotografiasInAlbum", albumId));
	}

	@Override
	public void copyFotoFromEventoToAlbum(Long eventoId, Long fotoId,
			Album album) throws Exception {
		executor.execute(new CopyFotoFromEventoToAlbumCommand(eventoId, fotoId,
				album));
	}

	@Override
	public void deleteFotoFromAlbum(Long albumId, Long fotoId)
			throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public Long obtenerNumMedioFotografiasPorAlbum() throws Exception {
		return (Long) executor.execute(new DirectDaoCommand("AlbumDao",
				"fotografiasMediaPorAlbum"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlbumFilterPhoto> albumsByNumFotografias(Long num_fotografias)
			throws Exception {
		return (List<AlbumFilterPhoto>) executor.execute(new DirectDaoCommand(
				"AlbumDao", "albumsByNumFotografias", num_fotografias));
	}
}
