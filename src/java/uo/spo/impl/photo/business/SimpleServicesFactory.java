package uo.spo.impl.photo.business;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.business.ServicesFactory;
import uo.spo.photo.business.UsuarioRegistradoServices;

public class SimpleServicesFactory implements ServicesFactory {

	@Override
	public FotografoServices getFotografoServices() {
		return new FotografoServicesImpl();
	}

	@Override
	public ClienteServices getClienteServices() {
		return new ClienteServicesImpl();
	}

	@Override
	public UsuarioRegistradoServices getUsuarioRegistradoServices() {
		return new UsuarioRegistradoServicesImpl();
	}

}
