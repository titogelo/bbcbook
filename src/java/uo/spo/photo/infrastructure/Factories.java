package uo.spo.photo.infrastructure;

import uo.spo.impl.photo.business.commands.CommandExecutorFactory;
import uo.spo.photo.business.ServicesFactory;
import uo.spo.photo.persistence.PersistenceFactory;

/**
 * Esta clase es la que realemente relaciona las interfaces de las capas con sus
 * implementaciones finales.
 * 
 * Se toma la configración de un fichero de propiedades de manera que cambiar la
 * configuración de la aplicación (cambiar de implementación en alguna capa)
 * quedará ahora reducido a cambiar el contenido del fichero
 * "factories.properties".
 * 
 * Este fichero, para esta implementación debe tener al menos estas dos
 * propiedades SERVICES_FACTORY y PERSISTENCE_FACTORY
 * 
 * SERVICES_FACTORY = z.impl.auction.business.SimpleServicesFactory
 * PERSISTENCE_FACTORY = z.impl.auction.persistence.JpaPersistenceFactory
 * COMMAND_EXECUTOR_FACTORY = z.impl.auction.persistence.JpaPersistenceFactory
 * 
 * Hay frameworks especializados en esto precisamente, por ejemplo Spring.
 * 
 */
public class Factories {
	private static String CONFIG_FILE = "/factories.properties";

	public static ServicesFactory services = (ServicesFactory) Helper
			.createFactory(CONFIG_FILE, "SERVICES_FACTORY");

	public static PersistenceFactory persistence = (PersistenceFactory) Helper
			.createFactory(CONFIG_FILE, "PERSISTENCE_FACTORY");

	public static CommandExecutorFactory executor = (CommandExecutorFactory) Helper
			.createFactory(CONFIG_FILE, "COMMAND_EXECUTOR_FACTORY");
}
