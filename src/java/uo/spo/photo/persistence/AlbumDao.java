package uo.spo.photo.persistence;

import java.util.List;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.AlbumFilterPhoto;

/**
 * Operaciones DAO relacionadas con la entidad Album
 * 
 * @see Album
 */
public interface AlbumDao extends GenericDao<Album> {

	public Long countNumberOfFotografiasInAlbum(Long albumId) throws Exception;

	public Long fotografiasMediaPorAlbum() throws Exception;

	public List<AlbumFilterPhoto> albumsByNumFotografias(Long num_fotografias)
			throws Exception;

	public List<Album> findAlbumByCliente(Long clienteId) throws Exception;

	public void updateEventAndAlbum(Long albumId) throws Exception;

}
