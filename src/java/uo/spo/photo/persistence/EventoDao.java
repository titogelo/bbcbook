package uo.spo.photo.persistence;

import java.util.List;

import uo.spo.photo.model.Evento;

/**
 * Operaciones DAO relacionadas con la entidad Evento
 * 
 * @see Evento
 */
public interface EventoDao extends GenericDao<Evento> {

	public List<Evento> getEventosByFotografo(Long fotografoId)
			throws Exception;

	public List<Evento> findEventosByCliente(Long clienteId) throws Exception;

}
