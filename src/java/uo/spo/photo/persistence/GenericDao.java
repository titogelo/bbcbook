package uo.spo.photo.persistence;

import java.util.List;

/**
 * 
 * Interface compartido por todos los datos de negocio.
 * 
 * Todas las operaciones CRUD estan aisladas en este interface y son compartidas
 * por todas las implementaciones DAO
 * 
 **/
public interface GenericDao<T> {

	T findById(Long id) throws Exception;

	List<T> findAll() throws Exception;

	T persist(T entity) throws Exception;

	T merge(T entity) throws Exception;

	void remove(T entity) throws Exception;

	void lock(T entity);

	void refresh(T entity);
}
