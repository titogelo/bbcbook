package uo.spo.photo.persistence;

import java.util.List;

import uo.spo.photo.model.Fotografia;

/**
 * Operaciones DAO relacionadas con la entidad Fotografia
 * 
 * @see Fotografia
 */
public interface FotografiaDao extends GenericDao<Fotografia> {

	public List<Fotografia> findFotografiaByEvento(Long eventoId)
			throws Exception;

	public List<Fotografia> findFotografiaByAlbum(Long albumId)
			throws Exception;

}
