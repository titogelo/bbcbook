package uo.spo.photo.persistence;

/**
 * Interfaz de la factoria que suministra implementaciones reales de la fachada
 * de persistencia para cada Entidad del Model
 */
public interface PersistenceFactory {

	FotografoDao getFotografoDao();

	ClienteDao getClienteDao();

	EventoDao getEventoDao();

	FotografiaDao getFotografiaDao();

	AlbumDao getAlbumDao();

	UsuarioDao getUsuarioDao();

}
