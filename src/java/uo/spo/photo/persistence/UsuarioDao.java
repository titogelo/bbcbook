package uo.spo.photo.persistence;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.Usuario;

/**
 * Operaciones DAO relacionadas con la entidad Usuario
 * 
 * @see Album
 */
public interface UsuarioDao extends GenericDao<Usuario> {

}
