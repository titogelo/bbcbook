package uo.spo.photo.persistence;

import uo.spo.photo.model.Fotografo;

/**
 * Operaciones DAO relacionadas con la entidad Fotografo
 * 
 * @see Fotografo
 */
public interface FotografoDao extends GenericDao<Fotografo> {

	public Fotografo validateLogin(Fotografo fotografo) throws Exception;

}
