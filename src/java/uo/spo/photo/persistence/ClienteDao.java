package uo.spo.photo.persistence;

import uo.spo.photo.model.Cliente;

/**
 * Operaciones DAO relacionadas con la entidad Cliente
 * 
 * @see Cliente
 */
public interface ClienteDao extends GenericDao<Cliente> {

	public Cliente validateLogin(Cliente cliente) throws Exception;

}
