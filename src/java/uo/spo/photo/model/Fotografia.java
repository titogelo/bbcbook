package uo.spo.photo.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;

import uo.spo.photo.business.BusinessException;

/**
 * Representacion de las fotografias de la aplicacion
 * 
 */
@Entity
public class Fotografia implements Serializable, Comparable<Fotografia> {
	@Id
	@GeneratedValue
	private Long id = null;

	@Version
	private int version = 0;

	private String nombre;

	private Date created = new Date();

	@ManyToMany
	private Set<Album> albums = new HashSet<Album>();

	@ManyToMany
	private Set<Evento> eventos = new HashSet<Evento>();

	private Resolucion resolucion;

	public Fotografia() {
	}

	public Fotografia(String nombre, Album album, Evento evento) {
		this.nombre = nombre;

		if (album != null){

			this.albums.add(album);
		}
		
		if (evento != null){
			this.eventos.add(evento);
			
		}
			

		this.created = Calendar.getInstance().getTime();
	}

	// ********************** Accessor Methods ********************** //

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void addAlbum(Album album) {
		if (album == null)
			throw new IllegalArgumentException(
					"No es posible añadir la fotografia a un album null.");
		albums.add(album);
	}

	public void removeAlbum(Album album) throws BusinessException {
		if (album == null)
			throw new IllegalArgumentException(
					"No es posible eliminar una fotografia de un album null.");

		if (albums.size() > 0) {
			albums.remove(album);
		} else {
			throw new BusinessException(
					"Es necesario tener fotografias en el album para eliminar alguna.");
		}
	}

	public Set<Album> getAlbums() {
		return albums;
	}

	public void addEvento(Evento evento) {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible añadir la fotografia a un evento null.");
		eventos.add(evento);
	}

	public void removeEvento(Evento evento) throws BusinessException {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible eliminar una fotografia de un evento null.");

		if (eventos.size() > 0) {
			eventos.remove(evento);
		} else {
			throw new BusinessException(
					"Es necesario tener fotografias en el evento para eliminar alguna.");
		}
	}

	public Set<Evento> getEventos() {
		return eventos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((albums == null) ? 0 : albums.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((eventos == null) ? 0 : eventos.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result
				+ ((resolucion == null) ? 0 : resolucion.hashCode());
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fotografia other = (Fotografia) obj;
		if (albums == null) {
			if (other.albums != null)
				return false;
		} else if (!albums.equals(other.albums))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (eventos == null) {
			if (other.eventos != null)
				return false;
		} else if (!eventos.equals(other.eventos))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (resolucion == null) {
			if (other.resolucion != null)
				return false;
		} else if (!resolucion.equals(other.resolucion))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Fotografia ('" + getId() + "'), " + "Nombre: '" + getNombre()
				+ "'";
	}

	@Override
	public int compareTo(Fotografia o) {
		// Don't compare Date objects! Use the time in milliseconds!
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = 1681912389090831439L;

	public Resolucion getResolucion() {
		return resolucion;
	}

	public void setResolucion(Resolucion resolucion) {
		this.resolucion = resolucion;
	}

}
