package uo.spo.photo.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import uo.spo.photo.business.BusinessException;

/**
 * Representacion del album
 * 
 */
@Entity
public class Album implements Serializable, Comparable<Album> {
	@Id
	@GeneratedValue
	private Long id = null;
	@Version
	private int version = 0;

	private String nombre;

	@ManyToOne
	private Cliente cliente;

	private Date created = new Date();

	private int num_fotos;

	@ManyToMany(mappedBy = "albums", fetch = FetchType.EAGER)
	private Set<Fotografia> fotografias = new HashSet<Fotografia>();

	public Album() {
	}

	/**
	 * @param nombre
	 * @param cliente
	 * @param num_fotos
	 * */
	public Album(String nombre, Cliente cliente, int num_fotos) {
		this.nombre = nombre;
		this.cliente = cliente;
		this.num_fotos = num_fotos;
		this.created = Calendar.getInstance().getTime();
	}

	// ********************** Accessor Methods ********************** //

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getCreated() {
		return created;
	}

	public int getNum_fotos() {
		return num_fotos;
	}

	public void setNum_fotos(int num_fotos) {
		this.num_fotos = num_fotos;
	}

	public void addFotografia(Fotografia fotografia) {
		if (fotografia == null)
			throw new IllegalArgumentException(
					"No es posible añadir una fotografia null.");
		fotografias.add(fotografia);
	}

	public void removeFotografia(Fotografia fotografia)
			throws BusinessException {
		if (fotografia == null)
			throw new IllegalArgumentException(
					"No es posible elimnar una fotografia null.");

		if (fotografias.size() > 0) {
			fotografias.remove(fotografia);
		} else {
			throw new BusinessException(
					"Es necesario tener fotografias para eliminar alguna.");
		}
	}

	public Set<Fotografia> getFotografia() {
		return fotografias;
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Album))
			return false;

		final Album album = (Album) o;

		if (!(created.getTime() == album.created.getTime()))
			return false;
		if (nombre != null ? !nombre.equals(album.nombre)
				: album.nombre != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (nombre != null ? nombre.hashCode() : 0);
		result = 29 * result + created.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Album ('" + getId() + "'), " + "Nombre: '" + getNombre() + "'";
	}

	@Override
	public int compareTo(Album o) {
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = -6350343222795235476L;

}
