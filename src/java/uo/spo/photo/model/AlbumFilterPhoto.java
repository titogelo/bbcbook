package uo.spo.photo.model;

/**
 * 
 * Se trata de una clase auxiliar para manejar adecuadamente los resultados de
 * una consulta. El concepto se denomina instanciación dinámica, y consiste en
 * tener una clase de apoyo con los mismo atributos que columnas vayas a
 * retornar en la select. De esta manera tendras en un objeto los datos, y
 * podrás manipularlos de manera más cómoda.
 * 
 * 
 * @author Angel
 * 
 * */
public class AlbumFilterPhoto {
	public String nombreAlbum;
	public long numeroDeFotografias;

	public AlbumFilterPhoto(String nombre) {
		nombreAlbum = nombre;

	}

	public String getNombreAlbum() {
		return nombreAlbum;
	}

	public void setNombreAlbum(String nombreAlbum) {
		this.nombreAlbum = nombreAlbum;
	}

	public long getNumeroDeFotografias() {
		return numeroDeFotografias;
	}

	public void setNumeroDeFotografias(int numeroDeFotografias) {
		this.numeroDeFotografias = numeroDeFotografias;
	}
}
