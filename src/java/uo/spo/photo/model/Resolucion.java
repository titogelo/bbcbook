package uo.spo.photo.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Resolucion de las fotografias que se usan en la aplicacion
 * 
 */
@Embeddable
public class Resolucion implements Serializable {

	private String alto;
	private String ancho;

	public Resolucion(String alto, String ancho) {
		super();
		this.alto = alto;
		this.ancho = ancho;
	}

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public Resolucion() {
	}

	private static final long serialVersionUID = -6846920608755104139L;

	public String getAlto() {
		return alto;
	}

	public void setAlto(String alto) {
		this.alto = alto;
	}

	public String getAncho() {
		return ancho;
	}

	public void setAncho(String ancho) {
		this.ancho = ancho;
	}

	@Override
	public String toString() {
		return "Resolucion [alto=" + alto + ", ancho=" + ancho + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alto == null) ? 0 : alto.hashCode());
		result = prime * result + ((ancho == null) ? 0 : ancho.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resolucion other = (Resolucion) obj;
		if (alto == null) {
			if (other.alto != null)
				return false;
		} else if (!alto.equals(other.alto))
			return false;
		if (ancho == null) {
			if (other.ancho != null)
				return false;
		} else if (!ancho.equals(other.ancho))
			return false;
		return true;
	}
}
