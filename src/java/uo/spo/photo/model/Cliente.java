package uo.spo.photo.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import uo.spo.photo.business.BusinessException;

/**
 * Representacion del cliente de la aplicacion
 * 
 */
@Entity
public class Cliente extends Usuario implements Serializable {

	@OneToMany(mappedBy = "cliente")
	private Set<Evento> eventos = new HashSet<Evento>();

	@OneToMany(mappedBy = "cliente")
	private Set<Album> albums = new HashSet<Album>();

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public Cliente() {
		super();
	}

	/**
	 * Full constructor
	 */
	public Cliente(String firstname, String lastname, String username,
			String password, String email, Direccion homeAddress) {
		super(firstname, lastname, username, password, email, homeAddress,
				false);
	}

	/**
	 * Simple constructor.
	 */
	public Cliente(String firstname, String lastname, String username,
			String password, String email) {
		super(firstname, lastname, username, password, email, false);

	}

	// ********************** Accessor Methods ********************** //

	public void addAlbum(Album album) {
		if (album == null)
			throw new IllegalArgumentException(
					"No es posible añadir un album null.");
		albums.add(album);
	}

	public Set<Album> getAlbums() {
		return albums;
	}

	public Collection<Evento> getEventos() {
		return eventos;
	}

	public void addEvento(Evento evento) {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible añadir un evento null.");
		eventos.add(evento);
	}

	public void removeEvento(Evento evento) throws BusinessException {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible elimnar un evento null.");

		if (eventos.size() > 0) {
			eventos.remove(evento);
		} else {
			throw new BusinessException(
					"Es necesario tener eventos para eliminar alguno.");
		}
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Cliente))
			return false;
		final Cliente user = (Cliente) o;
		return getLogin().equals(user.getLogin());
	}

	@Override
	public int hashCode() {
		return getLogin().hashCode();
	}

	@Override
	public String toString() {
		return "Cliente [" + getNombre() + " " + getApellidos() + ",con id: "
				+ getId() + " y login " + getLogin() + ", getEmail()="
				+ getEmail() + "]\n";
	}

	public int compareTo(Cliente o) {
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = -6350343222795235476L;

	// ********************** Business Methods ********************** //

}
