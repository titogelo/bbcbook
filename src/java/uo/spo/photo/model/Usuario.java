package uo.spo.photo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Version;

/**
 * Representacion de la entidad generica de usuario de la aplicacion
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuario implements Serializable, Comparable<Usuario> {

	@Id
	@GeneratedValue
	private Long id = null;

	@Version
	private int version = 0;

	@Column(unique = true, nullable = false, updatable = false)
	private String login; // Unique and immutable

	private String nombre;
	private String apellidos;

	private String password;
	private String email;

	private boolean admin = false;

	private Direccion direccion;

	private Date created = new Date();

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public Usuario() {
	}

	/**
	 * Full constructor
	 */
	public Usuario(String firstname, String lastname, String username,
			String password, String email, Direccion homeAddress, boolean admin) {
		this.nombre = firstname;
		this.apellidos = lastname;
		this.login = username;
		this.password = password;
		this.email = email;
		this.direccion = homeAddress;
		this.admin = admin;
	}

	/**
	 * Simple constructor.
	 */
	public Usuario(String firstname, String lastname, String username,
			String password, String email, boolean admin) {
		this.nombre = firstname;
		this.apellidos = lastname;
		this.login = username;
		this.password = password;
		this.email = email;
		this.admin = admin;
	}

	// ********************** Accessor Methods ********************** //

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String firstname) {
		this.nombre = firstname;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String lastname) {
		this.apellidos = lastname;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Direccion getAddress() {
		return direccion;
	}

	public void setAddress(Direccion homeAddress) {
		this.direccion = homeAddress;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getCreated() {
		return created;
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Usuario))
			return false;
		final Usuario user = (Usuario) o;
		return getLogin().equals(user.getLogin());
	}

	@Override
	public int hashCode() {
		return getLogin().hashCode();
	}

	@Override
	public String toString() {
		return "Usuario ('" + getId() + "'), " + "Login: '" + getLogin() + "'";
	}

	@Override
	public int compareTo(Usuario o) {
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = -6350343222795235476L;

	public void setLogin(String login) {
		this.login = login;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean getAdmin() {
		return admin;
	}

	// ********************** Business Methods ********************** //

}
