package uo.spo.photo.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import uo.spo.photo.business.BusinessException;

/**
 * Representacion de los fotografos de la aplicacion
 * 
 */
@Entity
public class Fotografo extends Usuario implements Serializable {

	@OneToMany(mappedBy = "fotografo")
	private Set<Evento> eventos = new HashSet<Evento>();

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public Fotografo() {
		super();
	}

	/**
	 * Full constructor
	 */
	public Fotografo(String firstname, String lastname, String username,
			String password, String email, boolean admin, Direccion homeAddress) {
		super(firstname, lastname, username, password, email, homeAddress, true);
	}

	/**
	 * Simple constructor.
	 */
	public Fotografo(String firstname, String lastname, String username,
			String password, String email) {
		super(firstname, lastname, username, password, email, true);

	}

	// ********************** Accessor Methods ********************** //

	public Set<Evento> getEventos() {
		return eventos;
	}

	public void addEvento(Evento evento) {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible añadir un evento null.");
		eventos.add(evento);
	}

	public void removeEvento(Evento evento) throws BusinessException {
		if (evento == null)
			throw new IllegalArgumentException(
					"No es posible eliminar un evento null.");

		if (eventos.size() > 0) {
			eventos.remove(evento);
		} else {
			throw new BusinessException(
					"Es necesario tener eventos para eliminar alguno.");
		}
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Fotografo))
			return false;
		final Fotografo user = (Fotografo) o;
		return getLogin().equals(user.getLogin());
	}

	@Override
	public int hashCode() {
		return getLogin().hashCode();
	}

	@Override
	public String toString() {
		return "Fotografo [" + getNombre() + " " + getApellidos() + ",con id: "
				+ getId() + " y login " + getLogin() + ", getEmail()="
				+ getEmail() + "]\n";
	}

	public int compareTo(Fotografo o) {
		// Don't compare Date objects! Use the time in milliseconds!
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = -6350343222795235476L;

	// ********************** Business Methods ********************** //

}
