package uo.spo.photo.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Direccion de un cliente
 * 
 */
@Embeddable
public class Direccion implements Serializable {
	private String calle;
	private String Zip;
	private String ciudad;

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public Direccion() {
	}

	/**
	 * Full constructor
	 */
	public Direccion(String street, String zipcode, String city) {
		this.calle = street;
		this.Zip = zipcode;
		this.ciudad = city;
	}

	// ********************** Accessor Methods ********************** //

	public String getStreet() {
		return calle;
	}

	public void setStreet(String street) {
		this.calle = street;
	}

	public String getZipcode() {
		return Zip;
	}

	public void setZipcode(String zipcode) {
		this.Zip = zipcode;
	}

	public String getCity() {
		return ciudad;
	}

	public void setCity(String city) {
		this.ciudad = city;
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Direccion))
			return false;

		final Direccion address = (Direccion) o;

		if (!ciudad.equals(address.ciudad))
			return false;
		if (!calle.equals(address.calle))
			return false;
		if (!Zip.equals(address.Zip))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = calle.hashCode();
		result = 29 * result + Zip.hashCode();
		result = 29 * result + ciudad.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Street: '" + getStreet() + "', " + "Zipcode: '" + getZipcode()
				+ "', " + "City: '" + getCity() + "'";
	}

	private static final long serialVersionUID = -6846920608755104139L;
}
