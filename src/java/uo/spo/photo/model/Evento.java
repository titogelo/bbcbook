package uo.spo.photo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import uo.spo.photo.business.BusinessException;

/**
 * Representacion de los eventos de la aplicacion
 * 
 */
@Entity
public class Evento implements Serializable, Comparable<Evento> {

	@Id
	@GeneratedValue
	private Long id = null;

	@Version
	private int version = 0;

	private String nombre;

	private Date created = new Date();

	@ManyToOne
	private Fotografo fotografo;

	@ManyToOne
	private Cliente cliente;

	@ManyToMany(mappedBy = "eventos")
	private Collection<Fotografia> fotografias = new ArrayList<Fotografia>();

	public Evento() {
	}

	/**
	 * @param nombre
	 * @param fotografo
	 * @param num_fotos
	 * */
	public Evento(String nombre, Fotografo fotografo, int num_fotos,
			Cliente cliente) {
		this.nombre = nombre;
		this.fotografo = fotografo;
		this.created = Calendar.getInstance().getTime();
		this.cliente = cliente;

	}

	// ********************** Accessor Methods ********************** //

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setFotografo(Fotografo fotografo) {
		this.fotografo = fotografo;
	}

	public Fotografo getFotografo() {
		return fotografo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void addFotografia(Fotografia fotografia) {
		if (fotografia == null)
			throw new IllegalArgumentException(
					"No es posible añadir una fotografia null.");
		fotografias.add(fotografia);
	}

	public void removeFotografia(Fotografia fotografia)
			throws BusinessException {
		if (fotografia == null)
			throw new IllegalArgumentException(
					"No es posible elimnar una fotografia null.");

		if (fotografias.size() > 0) {
			fotografias.remove(fotografia);
		} else {
			throw new BusinessException(
					"Es necesario tener fotografias para eliminar alguna.");
		}
	}

	public Collection<Fotografia> getFotografias() {
		return fotografias;
	}

	// ********************** Common Methods ********************** //

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Evento))
			return false;

		final Evento evento = (Evento) o;

		if (!(created.getTime() == evento.created.getTime()))
			return false;
		if (nombre != null ? !nombre.equals(evento.nombre)
				: evento.nombre != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (nombre != null ? nombre.hashCode() : 0);
		result = 29 * result + created.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "El evento con identificador [" + this.getId() + "] y nombre '"
				+ this.getNombre() + "', fue creado el: " + this.getCreated();
	}

	@Override
	public int compareTo(Evento o) {
		return Long.valueOf(this.getCreated().getTime()).compareTo(
				Long.valueOf(o.getCreated().getTime()));
	}

	private static final long serialVersionUID = -3994758042587513827L;
}
