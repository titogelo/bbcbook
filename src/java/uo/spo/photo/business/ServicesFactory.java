package uo.spo.photo.business;

public interface ServicesFactory {

	FotografoServices getFotografoServices();

	ClienteServices getClienteServices();

	UsuarioRegistradoServices getUsuarioRegistradoServices();

}