package uo.spo.photo.business;

import java.util.List;

import uo.spo.photo.model.Album;
import uo.spo.photo.model.AlbumFilterPhoto;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Usuario;

public interface ClienteServices {

	void albumUpdate(Album album) throws BusinessException, Exception;

	Album albumCreate(Album album) throws BusinessException, Exception;

	void albumDelete(Album album) throws BusinessException, Exception;

	void clienteUpdate(Cliente cliente) throws BusinessException, Exception;

	Album getFotosByAlbum(Long albumId) throws BusinessException, Exception;

	Evento getFotosByEvento(Long eventoId) throws BusinessException, Exception;

	List<Album> getAllAlbums() throws BusinessException, Exception;

	List<Evento> getAllEventos() throws BusinessException, Exception;

	List<Usuario> getAllClientes() throws BusinessException, Exception;

	Cliente getAlbumByCliente(Long clienteId) throws BusinessException,
			Exception;

	Cliente getEventoByCliente(Long clienteId) throws BusinessException,
			Exception;

	Album getAlbumById(Long albumId) throws BusinessException, Exception;

	Fotografia getFotografiaById(Long fotografiaId) throws BusinessException,
			Exception;

	Evento getEventoById(Long eventoId) throws BusinessException, Exception;

	Cliente getClienteById(Long clienteId) throws BusinessException, Exception;

	Long obtenerNumMedioFotografiasPorAlbum() throws BusinessException,
			Exception;

	List<AlbumFilterPhoto> albumsByNumFotografias(Long num_fotografias)
			throws BusinessException, Exception;

	int getNumFotografiasAlbum(Long albumId) throws BusinessException,
			Exception;

	void copyFotoFromEventoToAlbum(Long eventoId, Long fotoId, Album album)
			throws BusinessException, Exception;

	void deleteFotoFromAlbum(Long albumId, Long fotoId)
			throws BusinessException, Exception;

}
