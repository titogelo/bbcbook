package uo.spo.photo.business;

import java.util.List;

import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.model.Usuario;

public interface FotografoServices {

	void eventoUpdate(Evento evento) throws BusinessException, Exception;

	Evento eventoCreate(Evento evento) throws BusinessException, Exception;

	void eventoDelete(Evento evento) throws BusinessException, Exception;

	void fotografiaCreate(Fotografia fotografia) throws BusinessException,
			Exception;

	void fotografoUpdate(Fotografo fotografo) throws BusinessException,
			Exception;

	List<Evento> getAllEventos() throws BusinessException, Exception;

	List<Usuario> getAllFotografos() throws BusinessException, Exception;

	Evento getFotosByEvento(Long eventoId) throws BusinessException, Exception;

	Fotografo getEventoByFotografo(Long fotografoId) throws BusinessException,
			Exception;

	Evento getEventoById(Long eventoId) throws BusinessException, Exception;

	Fotografo getFotografoById(Long fotografoId) throws BusinessException,
			Exception;

	Cliente getClienteById(Long clienteId) throws BusinessException, Exception;
}
