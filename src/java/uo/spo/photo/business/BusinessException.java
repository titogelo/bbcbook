package uo.spo.photo.business;

/**
 * This exception is used to mark business rule violations.
 * 
 * @author Christian Bauer
 */
public class BusinessException extends Exception {

	public static final String ERROR_MSG = "business_error_msg";

	public BusinessException() {
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = -1199804998878017045L;
}
