package uo.spo.photo.business;

import uo.spo.photo.model.Usuario;

public interface UsuarioRegistradoServices {

	void userRegisterNew(Usuario usuario) throws BusinessException, Exception;

	Long fotografoLogin(String username, String pass) throws BusinessException,
			Exception;

	Long clienteLogin(String username, String pass) throws BusinessException,
			Exception;
}
