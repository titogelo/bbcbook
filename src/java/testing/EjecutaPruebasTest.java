package testing;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class EjecutaPruebasTest extends TestCase {

	/**
	 * Clase para ejecutar todo el suite de pruebas. A medida que se vayan
	 * haciendo pruebas se iran añadiendo.
	 * */
	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTest(new AlbumTest("testFiltrarAlbums"));
		suite.addTest(new EventoTest("testInsertaFotografiasEvento"));
		suite.addTest(new EventoTest("testCopiaFotografiasDeEventoAlbum"));
		return suite;
	}
}
