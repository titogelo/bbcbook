package testing;

import junit.framework.TestCase;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;

public class AlbumTest extends TestCase {

	Evento evento = null;
	Album album = null;
	Cliente cliente = null;
	Fotografo fotografo = null;

	FotografoServices fs = null;
	ClienteServices cs = null;

	public static final int NUMERO_FOTOGRAFIAS = 5;

	public AlbumTest(String name) {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();

		fs = Factories.services.getFotografoServices();
		cs = Factories.services.getClienteServices();

		cliente = new Cliente("Angel", "Suarez", "login1", "pass1",
				"angel@hotmail.com");
		fotografo = new Fotografo("Paco", "Solis", "paco1", "paco1",
				"paco@hotmail.com");
		evento = new Evento("Evento", fotografo, 0, cliente);

		album = new Album("Album", cliente, 0);

		evento = fs.eventoCreate(evento);

		album = cs.albumCreate(album);
	}

	/**
	 * Prueba 1: Comprobamos que la creacion de un album se produce de manera
	 * correcta
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testNuevoAlbum() throws BusinessException, Exception {

		// Creo un nuevo album aprovechando el cliente y el fotografo
		Album nuevo_album = new Album("Nuevo Album", cliente, 0);
		cs.albumCreate(nuevo_album);

		// Comprobamos que el album se ha creado. El id del nuevo album debe ser
		// igual que el que seleccionemos a traves de las busqueda
		assertNotNull("El album que se acaba de crear no existe",
				cs.getAlbumById(nuevo_album.getId()));
	}

	/**
	 * Prueba 2: Comprobamos que la insercion de fotografias en el album la hace
	 * correctamente. Despues de insertar comprobamos que el numero de elementos
	 * del album es mayor que cero e igual al numero de fotografias insertadasa.
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testInsertarFotosAlbum() throws BusinessException, Exception {

		for (int i = 0; i < NUMERO_FOTOGRAFIAS; i++) {
			Fotografia fotografia = new Fotografia("Fotos para copiar" + i,
					null, evento);
			fs.fotografiaCreate(fotografia);
		}

		System.out.println(evento.getFotografias().size());
				
		for (Fotografia foto : evento.getFotografias()) {
			cs.copyFotoFromEventoToAlbum(evento.getId(), foto.getId(), album);
		}
		
		System.out.println(album.getNum_fotos());
		
		assertTrue("Las fotos no se añadieron correctamente al album." +
				"", (album.getNum_fotos() > 0) && 
				(album.getNum_fotos() == NUMERO_FOTOGRAFIAS-1));
	}

	/**
	 * Prueba 1: Comprobamos que si metemos X fotografias cuando utilicemos la
	 * funcion de filtrar albums por el numero de fotografias debe aparecer
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testFiltrarAlbums() throws BusinessException, Exception {
		/*
		 * for (int i = 0; i < NUMERO_FOTOGRAFIAS; i++) { Fotografia fotografia
		 * = new Fotografia("Foto numero " + i, album, null);
		 * 
		 * fs.fotografiaCreate(fotografia); }
		 * 
		 * System.out.println(album.getNum_fotos());
		 * 
		 * List<AlbumFilterPhoto> album_con_mas_de_X = cs
		 * .albumsByNumFotografias((long) NUMERO_FOTOGRAFIAS - 1);
		 * 
		 * // A la espera que saber que poner ahi.
		 * assertTrue("El album creado con " + NUMERO_FOTOGRAFIAS +
		 * " no esta en el conjunto de " + "los albumes que tienen " +
		 * NUMERO_FOTOGRAFIAS + " fotografias.",
		 * album_con_mas_de_X.contains(album));
		 */
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}

}
