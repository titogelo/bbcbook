/**
 * Testing para Evento
 */
package testing;

import junit.framework.TestCase;
import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;

/**
 * @author Angel
 * 
 */
public class EventoTest extends TestCase {

	public EventoTest(String name) {
		super(name);
	}

	Evento evento = null;
	Album album = null;
	Cliente cliente = null;
	Fotografo fotografo = null;

	FotografoServices fs = null;
	ClienteServices cs = null;

	public static final int NUMERO_FOTOGRAFIAS = 5;

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		fs = Factories.services.getFotografoServices();
		cs = Factories.services.getClienteServices();

		cliente = new Cliente("Angel", "Suarez", "login1", "pass1",
				"angel@hotmail.com");
		fotografo = new Fotografo("Paco", "Solis", "paco1", "paco1",
				"paco@hotmail.com");
		evento = new Evento("Evento", fotografo, 0, cliente);

		album = new Album("Album", cliente, 0);

		evento = fs.eventoCreate(evento);

		album = cs.albumCreate(album);
	}

	/**
	 * Prueba 1: Comprobamos que si metemos X fotografias cuando las recuperemos
	 * habra las mismas, es decir X.
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testInsertaFotografiasEvento() throws BusinessException,
			Exception {

		for (int i = 0; i < NUMERO_FOTOGRAFIAS; i++) {
			Fotografia fotografia = new Fotografia("Foto numero " + i, null,
					evento);
			fs.fotografiaCreate(fotografia);
		}

		evento = fs.getFotosByEvento((long) evento.getId());

		assertEquals("El evento no tiene " + NUMERO_FOTOGRAFIAS
				+ " fotografias ", evento.getFotografias().size(),
				NUMERO_FOTOGRAFIAS);
	}

	/**
	 * Prueba 2: Comprobamos que metiendo X fotos en un evento y despues
	 * copiando las fotos a un album, en ese album tiene que haber exactamente
	 * el mismo numero de fotos.
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testCopiaFotografiasDeEventoAlbum() throws BusinessException,
			Exception {

		for (int i = 0; i < NUMERO_FOTOGRAFIAS; i++) {
			Fotografia fotografia = new Fotografia("Foto numero " + i, null,
					evento);
			fs.fotografiaCreate(fotografia);
		}

		evento = fs.getFotosByEvento((long) evento.getId());

		for (Fotografia foto : evento.getFotografias()) {
			cs.copyFotoFromEventoToAlbum(evento.getId(), foto.getId(), album);
		}

		album = cs.getFotosByAlbum((long) album.getId());

		assertEquals("El album no tiene " + NUMERO_FOTOGRAFIAS
				+ " fotografias ", album.getFotografia().size(),
				NUMERO_FOTOGRAFIAS);

	}

	/**
	 * Prueba 3: Comprobamos que la creacion de un evento se produce de manera
	 * correcta
	 * 
	 * @throws Exception
	 * @throws BusinessException
	 * */
	public void testNuevoEvento() throws BusinessException, Exception {

		// Creo un nuevo evento aprovechando el cliente y el fotografo
		Evento nuevo_evento = new Evento("Nuevo Evento", fotografo, 0, cliente);
		fs.eventoCreate(nuevo_evento);

		for (int i = 0; i < NUMERO_FOTOGRAFIAS; i++) {
			Fotografia fotografia = new Fotografia("Foto nueva evento" + i,
					null, nuevo_evento);
			fs.fotografiaCreate(fotografia);
		}

		// Comprobamos que el evento se ha creado. Id nuevo evento debe ser
		// igual que el que seleccionemos a traves de las busqueda
		assertNotNull("El evento que se acaba de crear no existe",
				fs.getEventoById(nuevo_evento.getId()));
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

	}

}
