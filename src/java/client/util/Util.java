package client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Metodos de utilidad para leer cosas de teclado con prompt
 * 
 * @author Angel
 * 
 */
public class Util {
	private static BufferedReader kbd = new BufferedReader(
			new InputStreamReader(System.in));

	public static String getString(String msg) {
		System.out.print(msg);
		try {
			return kbd.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Long getLong(String msg) {
		System.out.print(msg);
		Long res = null;
		try {
			String val = kbd.readLine();
			// Is empty line returns null
			if (val != null && !val.equals("")) {
				res = Long.valueOf(val);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return res;
	}

	public static Date getDate(String msg) {
		Date d = null;
		boolean error;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		try {
			do {
				error = false;
				System.out.print(msg + "(dd/mm/yyyy) ");
				String dateStr;
				dateStr = kbd.readLine();
				try {
					d = format.parse(dateStr);
				} catch (ParseException e) {
					error = true;
				}
			} while (error);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return d;
	}

	public static Integer getInteger(String msg) {
		System.out.print(msg);
		Integer res = null;
		try {
			String val = kbd.readLine();
			// Is empty line returns null
			if (val != null && !val.equals("")) {
				res = Integer.valueOf(val);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return res;
	}

	/*
	 * public static MonetaryAmount getMoney(String msg){ System.out.print(msg);
	 * MonetaryAmount m = null;
	 * 
	 * BigDecimal amount = new BigDecimal(getString("Cantidad (�): ")); Currency
	 * currency = Currency.getInstance("EUR");
	 * 
	 * m = new MonetaryAmount(amount, currency); return m;
	 * 
	 * }
	 * 
	 * public static BigDecimal getBigDecimal(String msg) {
	 * System.out.print(msg); BigDecimal res = null; try { String val =
	 * kbd.readLine(); // Is empty line returns null if (val != null &&
	 * !val.equals("")){ res = BigDecimal.valueOf(Double.valueOf(val)); } }
	 * catch (IOException e) { throw new RuntimeException(e); } return res; }
	 */
}
