package client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uo.spo.photo.business.BusinessException;

/**
 * Manages menu and its options. Must be redefined with a concret menu and its
 * options processing
 * 
 * @author Angel
 */
public abstract class ConsoleMain {
	protected static final int EXIT = 0;
	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	protected String[] menuOptionsCliente;
	protected String[] menuOptionsFotografo;

	public ConsoleMain() {
		super();
	}

	protected abstract void processOption(int option) throws BusinessException,
			Exception;

	protected void run() throws Exception {
		int opt;

		do {
			showMenu();
			opt = getMenuOption();
			try {
				processOption(opt);
			} catch (BusinessException be) {
				Printer.printBusinessException(be);
			}
		} while (opt != EXIT);

		System.out.println("STOP RUN");
	}

	protected int getMenuOption() {
		int opt;
		do {
			System.out.print("Opcion: ");
			try {
				opt = Integer.parseInt(kbd.readLine());
			} catch (NumberFormatException nfe) {
				opt = -1;
			} catch (IOException ioe) {
				throw new RuntimeException(ioe);
			}
		} while (opt < EXIT);

		return opt;
	}

	protected void showMenu() {
		if (ClientSession.getTipousuario() == 1) {
			for (String s : menuOptionsCliente) {
				System.out.println(s);
			}
		} else {
			for (String s : menuOptionsFotografo) {
				System.out.println(s);
			}
		}
	}

}