package client.util;

/**
 * Emplea un TjreadLocal para guardar el ID del usuario cargado durante el LOGIN
 * y que se necesitar� en algunas operaciones.
 * 
 * Se podria extender para guardar m�s informaci�n �til
 * 
 * @author Angel
 */
public class ClientSession {
	private static final ThreadLocal<Long> loginId = new ThreadLocal<Long>();
	private static final ThreadLocal<Long> tipoUsuario = new ThreadLocal<Long>();

	public static void setLoginId(Long id) {
		loginId.set(id);
	}

	public static Long getLoginId() {
		return loginId.get();
	}

	public static Long getTipousuario() {
		return tipoUsuario.get();
	}

	public static void setTipousuario(Long tipo) {
		tipoUsuario.set(tipo);
	}
}
