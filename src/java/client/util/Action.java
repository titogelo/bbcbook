package client.util;

import uo.spo.photo.business.BusinessException;

/**
 * Representa cada acci�n invocada por el usuario.
 * 
 * Cada acci�n se encargar� de la interacci�n con el usuario: pantalla, teclado,
 * listados y validaciones; e invocar� a la capa de servicios.
 * 
 * @author Angel
 */
public interface Action {
	void execute() throws BusinessException, Exception;
}
