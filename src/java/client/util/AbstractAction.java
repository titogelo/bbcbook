package client.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uo.spo.photo.business.BusinessException;

/**
 * Implementa el interfaz Action y da log para todas sus clases herederas
 * 
 * @author Angel
 */
public abstract class AbstractAction implements Action {
	protected Logger log;

	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	public AbstractAction() {
		log = LoggerFactory.getLogger(this.getClass());
	}

	@Override
	public abstract void execute() throws BusinessException, Exception;

}
