package client.fotografo;

import uo.spo.photo.business.BusinessException;
import client.fotografo.actions.AdminLogin;
import client.fotografo.actions.ConsultarCliente;
import client.fotografo.actions.CreateEvento;
import client.fotografo.actions.EliminarAlbum;
import client.fotografo.actions.EliminarEvento;
import client.fotografo.actions.ListarClientes;
import client.fotografo.actions.ListarEventos;
import client.fotografo.actions.ListarEventosByFotografo;
import client.fotografo.actions.ListarFotografiasEventoByFotografo;
import client.fotografo.actions.ListarFotografos;
import client.fotografo.actions.ModificarCliente;
import client.fotografo.actions.ModificarEvento;
import client.fotografo.actions.ModificarFotografo;
import client.fotografo.actions.RegistrarCliente;
import client.fotografo.actions.RegistrarFotografo;
import client.usuario.CopiarFotoEventoAlbum;
import client.usuario.CreateAlbum;
import client.usuario.ListarAlbums;
import client.usuario.ListarAlbumsByCliente;
import client.usuario.ListarEventosByCliente;
import client.usuario.ListarFotografiasAlbumsByCliente;
import client.usuario.ObtenerAlbumConMasDeXFotografias;
import client.usuario.ObtenerNumMedioFotografiasPorAlbum;
import client.util.ConsoleMain;
import client.util.Printer;

/**
 * 
 * Esta clase es la que se ejecuta al inicio y es la que se encarga de ir
 * invocando a las distintas clases y distintos metodos.
 * 
 * He creado dos menus que estan vinculados a los dos distintos tipos de
 * usuarios que existen. Es decir hay un menu para los fotografos con
 * funcionalidades orientadas más a la administracion y un segundo menu para los
 * clientes con funcionalidades mas simples que le permiten manejar sus albumes.
 * 
 * @author Angel
 * 
 * */
public class Main extends ConsoleMain {

	public static void main(String[] args) throws Exception {
		new Main().run();
	}

	@Override
	public void run() throws Exception {
		try {
			new AdminLogin().execute();
			super.run();
		} catch (BusinessException bex) {
			Printer.printBusinessException(bex);
		} catch (RuntimeException rex) {
			Printer.printRuntimeException(rex);
		}
	}

	public Main() {

		menuOptionsCliente = new String[] {
				"Menu cliente",
				"\t 10- Crear album \t 16- Listar fotografias de un album",
				"\t 11- Eliminar album \t 17- Ver detalle de cliente",
				"\t 12- Listar eventos del cliente \t 18- Modificar datos de "
						+ "cliente",
				"\t 13- Copiar foto de evento a album \t 19- Obtener número "
						+ "de fotografías medio por album",
				"\t 14- Listar albums \t 20- Filtrar albums por numero de"
						+ " fotografias que contiene",
				"\t 15- Listar albums cliente", "0- Salir" };
		menuOptionsFotografo = new String[] {
				"Menú de fotografo",
				"\t 1- Crear evento \t 4- Listar eventos \t 7- Listar "
						+ "fotografos",
				"\t 2- Modificar evento \t 5- Listar eventos fotografo \t 8- "
						+ "Modificar datos fotografo ",
				"\t 3- Eliminar evento \t 6- Listar clientes \t 9- Listar "
						+ "fotografias de un evento", "",
				"\t 21- Crear Fotografo  \t 22- Crear cliente", "0- Salir" };

	}

	@Override
	protected void processOption(int option) throws Exception {
		switch (option) {
		case 1:
			new CreateEvento().execute();
			break;
		case 2:
			new ModificarEvento().execute();
			break;
		case 3:
			new EliminarEvento().execute();
			break;
		case 4:
			new ListarEventos().execute();
			break;
		case 5:
			new ListarEventosByFotografo().execute();
			break;
		case 6:
			new ListarClientes().execute();
			break;
		case 7:
			new ListarFotografos().execute();
			break;
		case 8:
			new ModificarFotografo().execute();
			break;
		case 9:
			new ListarFotografiasEventoByFotografo().execute();
			break;
		case 10:
			new CreateAlbum().execute();
			break;
		case 11:
			new EliminarAlbum().execute();
			break;
		case 12:
			new ListarEventosByCliente().execute();
			break;
		case 13:
			new CopiarFotoEventoAlbum().execute();
			break;
		case 14:
			new ListarAlbums().execute();
			break;
		case 15:
			new ListarAlbumsByCliente().execute();
			break;
		case 16:
			new ListarFotografiasAlbumsByCliente().execute();
			break;
		case 17:
			new ConsultarCliente().execute();
			break;
		case 18:
			new ModificarCliente().execute();
			break;
		case 19:
			new ObtenerNumMedioFotografiasPorAlbum().execute();
			break;
		case 20:
			new ObtenerAlbumConMasDeXFotografias().execute();
			break;
		case 21:
			new RegistrarFotografo().execute();
			break;
		case 22:
			new RegistrarCliente().execute();
			break;
		}
	}
}
