package client.fotografo.actions;

import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Fotografo;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos modificar los
 * datos personales de un fotografo.
 * 
 * Tras la comprobacion previa, de la existencia del fotografo que se desea
 * modificar, se realiza la modificacion.
 * 
 * @author Angel
 * 
 * */
public class ModificarFotografo extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long idFotografo = ClientSession.getLoginId();

		FotografoServices us = Factories.services.getFotografoServices();

		Fotografo fotografo = us.getFotografoById(idFotografo);

		while (fotografo == null) {
			idFotografo = Util.getLong("Introduce un id de fotografo valido "
					+ "para modificar: ");
			fotografo = us.getFotografoById(idFotografo);
		}

		String nombre = Util.getString("Nombre nuevo: ");
		String apellidos = Util.getString("Apellidos nuevos: ");

		fotografo.setApellidos(apellidos);
		fotografo.setNombre(nombre);

		us.fotografoUpdate(fotografo);

	}
}
