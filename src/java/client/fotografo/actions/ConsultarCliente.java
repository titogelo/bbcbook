package client.fotografo.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * Esta clase permite visualizar los datos de un cliente.
 * 
 * Se realizan unas comprobaciones para verificar que el usuario introduce un
 * numero de identificador correcto y en ultima instancia se obtienen los datos
 * del cliente a traves de su ID
 * 
 * @author Angel
 * 
 * */
public class ConsultarCliente extends AbstractAction {
	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	@Override
	public void execute() throws Exception {

		Long idCliente;

		ClienteServices us = Factories.services.getClienteServices();

		Cliente cliente = null;

		System.out.print("Id cliente a consultar: ");

		do {
			do {

				try {
					idCliente = (long) Integer.parseInt(kbd.readLine());
				} catch (NumberFormatException nfe) {
					System.out.print("Intentelo de nuevo: ");
					idCliente = (long) -1;
				} catch (IOException ioe) {
					System.out.print("Intentelo de nuevo: ");
					throw new RuntimeException(ioe);
				}
			} while (idCliente < 0);

			cliente = us.getClienteById(idCliente);

		} while (cliente == null);

		cliente = us.getClienteById(idCliente);

		Printer.print(cliente.toString());

	}
}
