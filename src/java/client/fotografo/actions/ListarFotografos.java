package client.fotografo.actions;

import java.util.List;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Usuario;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos listar los
 * fotografos que han sido dados de alta en la aplicacion.
 * 
 * Sin restriccion de ningun tipo se listan todos los fotografos que han sido
 * dados de alta
 * 
 * @author Angel
 * 
 * */
public class ListarFotografos extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Printer.print("Todos los fotografos dados de alta hasta el"
				+ " momento son: ");

		List<Usuario> lista = Factories.services.getFotografoServices()
				.getAllFotografos();

		for (Usuario usuario : lista) {
			Printer.print(" - " + usuario.toString());
		}

	}
}
