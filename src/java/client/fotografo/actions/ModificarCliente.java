package client.fotografo.actions;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import client.util.AbstractAction;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos modificar los
 * datos personales de un cliente.
 * 
 * Tras la comprobacion previa, de la existencia del cliente que se desea
 * modificar, se realiza la modificacion.
 * 
 * @author Angel
 * 
 * */
public class ModificarCliente extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long idCliente = Util.getLong("Id cliente a modificar: ");

		ClienteServices us = Factories.services.getClienteServices();

		Cliente cliente = us.getClienteById(idCliente);

		while (cliente == null) {
			idCliente = Util.getLong("Introduce un id de cliente valido para "
					+ "modificar: ");
			cliente = us.getClienteById(idCliente);
		}

		String nombre = Util.getString("Nombre nuevo: ");
		String apellidos = Util.getString("Apellidos nuevos: ");

		cliente.setApellidos(apellidos);
		cliente.setNombre(nombre);

		us.clienteUpdate(cliente);

	}
}
