package client.fotografo.actions;

import java.util.List;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Evento;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos listar eventos
 * que hayan sido creados en toda la aplicacion.
 * 
 * Al igual que los clientes en esta clase listamos todos los eventos creados
 * sin ningun tipo de restriccion
 * 
 * @author Angel
 * 
 * */
public class ListarEventos extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Printer.print("Todos los evento creados hasta el momento son: ");

		List<Evento> lista = Factories.services.getFotografoServices()
				.getAllEventos();

		for (Evento evento : lista) {
			Printer.print(" - " + evento.toString());
		}

	}
}
