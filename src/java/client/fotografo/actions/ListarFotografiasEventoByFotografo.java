package client.fotografo.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos listar las
 * fotografias de los eventos que han sido creados por el fotografo que se
 * encuentra en sesion.
 * 
 * Aqui vamos un poco mas allá y mostramos ademas de los eventos del cliente
 * todas las fotografias que pertenecen a dichos eventos y ademas teniendo en
 * cuenta el evento al que pertenecen
 * 
 * @author Angel
 * 
 * */
public class ListarFotografiasEventoByFotografo extends AbstractAction {

	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	@Override
	public void execute() throws Exception {

		Long idFotografo = ClientSession.getLoginId();

		FotografoServices us = Factories.services.getFotografoServices();

		Fotografo temporal = us.getFotografoById(idFotografo);

		while (temporal == null) {
			idFotografo = Util
					.getLong("Por favor, introduzca un ID de fotografo "
							+ "válido: ");

			temporal = us.getFotografoById(idFotografo);
		}

		Printer.print("Estos son sus eventos: ");

		Fotografo fotografo = us.getEventoByFotografo(idFotografo);

		for (Evento evento : fotografo.getEventos()) {
			Printer.print(" - " + evento.toString());
		}

		Long idEvento;
		Evento evento = null;

		System.out.print("Seleccione el id del evento del que desea"
				+ " visualizar sus fotografias: ");

		do {
			do {

				try {
					idEvento = (long) Integer.parseInt(kbd.readLine());
				} catch (NumberFormatException nfe) {
					System.out.print("Intentelo de nuevo: ");
					idEvento = (long) -1;
				} catch (IOException ioe) {
					System.out.print("Intentelo de nuevo: ");
					throw new RuntimeException(ioe);
				}
			} while (idEvento < 0);

			evento = us.getEventoById(idEvento);

		} while (evento == null);

		evento = us.getFotosByEvento(idEvento);

		if (evento.getFotografias().size() == 0) {
			Printer.print("Este evento no tiene fotografias.");
		} else {
			for (Fotografia fotografia : evento.getFotografias()) {
				Printer.print(fotografia.toString());
			}
		}

	}
}
