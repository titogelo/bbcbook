package client.fotografo.actions;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Direccion;
import uo.spo.photo.model.Usuario;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos dar de alta
 * nuevos clientes que puedan utilizar la aplicacion.
 * 
 * @author Angel
 * 
 * */
public class RegistrarCliente {

	public void execute() throws Exception {

		if (ClientSession.getTipousuario() == 2) {
			String nombre = Util.getString("Nombre de cliente: ");
			String apellidos = Util.getString("Apellidos: ");
			String login = Util.getString("Login: ");
			String pass = Util.getString("Password: ");
			String zip = Util.getString("Zip: ");
			String calle = Util.getString("Calle: ");
			String ciudad = Util.getString("Ciudad: ");
			String email = Util.getString("Email: ");

			Direccion direccion = new Direccion(calle, zip, ciudad);

			Usuario usuario = new Cliente(nombre, apellidos, login, pass,
					email, direccion);

			Factories.services.getUsuarioRegistradoServices().userRegisterNew(
					usuario);

		} else {
			Printer.print("No eres fotografo no puedes registrar a nadie.");
		}

	}

}
