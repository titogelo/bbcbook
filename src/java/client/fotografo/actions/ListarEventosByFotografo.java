package client.fotografo.actions;

import java.util.List;

import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografo;
import client.util.AbstractAction;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos listar eventos
 * que hayan sido creados por el fotografo que se encuentra en la sesion actual.
 * 
 * Obteniendo el ID del fotografo que esta en sesion listamos todos sus eventos.
 * 
 * @author Angel
 * 
 * */
public class ListarEventosByFotografo extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long idFotografo = Util.getLong("Introduzca el ID del fotografo: ");

		FotografoServices us = Factories.services.getFotografoServices();

		Fotografo temporal = us.getFotografoById(idFotografo);

		while (temporal == null) {
			idFotografo = Util
					.getLong("Por favor, introduzca un ID de fotografo "
							+ "válido: ");

			us = Factories.services.getFotografoServices();

			temporal = us.getFotografoById(idFotografo);
		}

		Printer.print("Los evento creados por el señor "
				+ temporal.getApellidos() + " hasta el momento son: ");

		List<Evento> lista = Factories.services.getFotografoServices()
				.getAllEventos();

		for (Evento evento : lista) {
			Printer.print(" - " + evento.toString());
		}
	}
}
