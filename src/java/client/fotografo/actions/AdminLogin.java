package client.fotografo.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uo.spo.photo.infrastructure.Factories;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase gestiona el acceso a la aplicacion.
 * 
 * En funcion del tipo de acceso que se demande validaremos un tipo de usuario y
 * otro y por lo tanto se motrara el menu que tenga asociado ese tipo de
 * usuario.
 * 
 * @author Angel
 * 
 * */
public class AdminLogin {

	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	public void execute() throws Exception {

		Printer.print("¿Como deseas loguearte? Cliente (1) o Fotografo (2)");

		Long tipo = null;

		do {
			System.out.print("Tipo: ");
			try {
				tipo = (long) Integer.parseInt(kbd.readLine());
			} catch (NumberFormatException nfe) {
				tipo = (long) -1;
			} catch (IOException ioe) {
				throw new RuntimeException(ioe);
			}
		} while (tipo < 0);

		if (tipo == 1) {
			Printer.print("Login valido para cliente: 'login0' para el "
					+ "usuario y 'pass0' para la password");

			String username = Util.getString("Usuario: ");
			String pass = Util.getString("Password: ");

			Long userId = Factories.services.getUsuarioRegistradoServices()
					.clienteLogin(username, pass);

			Printer.print("Tu id cliente es " + userId);

			ClientSession.setTipousuario(tipo);
			ClientSession.setLoginId(userId);

		} else {
			Printer.print("Login valido para fotografo: 'pacomoreno' para el "
					+ "usuario y 'pacomoreno' para la password");

			String username = Util.getString("Usuario: ");
			String pass = Util.getString("Password: ");

			Long userId = Factories.services.getUsuarioRegistradoServices()
					.fotografoLogin(username, pass);

			Printer.print("Tu id de fotografo es " + userId);

			ClientSession.setTipousuario(tipo);
			ClientSession.setLoginId(userId);

		}

	}

}
