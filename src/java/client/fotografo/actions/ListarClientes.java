package client.fotografo.actions;

import java.util.List;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Usuario;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos listar los
 * clientes que han sido dados de alta en la aplicacion.
 * 
 * Sin restriccion de ningun tipo se listan todos los clientes que han sido
 * dados de alta
 * 
 * @author Angel
 * 
 * */
public class ListarClientes extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Printer.print("Todos los clientes dados de alta hasta el momento son: ");

		List<Usuario> lista = Factories.services.getClienteServices()
				.getAllClientes();

		for (Usuario usuario : lista) {
			Printer.print(" - " + usuario.toString());
		}

	}
}
