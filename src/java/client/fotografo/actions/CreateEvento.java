package client.fotografo.actions;

import java.io.IOException;

import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import uo.spo.photo.model.Fotografo;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos crear eventos.
 * 
 * Como era de esperar se realizan comprobaciones de todo tipo para llegar sin
 * ningun problema a la creacion de un evento. Para crear el evento se le piden
 * los datos que identifiquen el evento y a continuacion se pide al fotografo la
 * relacion de fotografias que desea incluir en dicho evento. Como es de esperar
 * tambien, en la inserccion de esas fotografias, se realizan las comprobaciones
 * pertinentes.
 * 
 * @author Angel
 * 
 * */
public class CreateEvento extends AbstractAction {

	@Override
	public void execute() throws Exception {
		String name = Util.getString("Nombre: ");
		Long idFotografo = ClientSession.getLoginId();

		FotografoServices us = Factories.services.getFotografoServices();

		Fotografo fotografo_temporal = us.getFotografoById(idFotografo);

		if (fotografo_temporal.equals(null))
			throw new BusinessException("El fotografo debe existir.");

		us = Factories.services.getFotografoServices();

		Long idCliente;

		Cliente cliente = null;

		System.out.print("Id del cliente destinatario del evento: ");
		int i = 0;
		do {
			do {
				if (i > 0) {
					Printer.print("Intentelo de nuevo: ");
				}
				try {
					idCliente = (long) Integer.parseInt(kbd.readLine());
				} catch (NumberFormatException nfe) {

					idCliente = (long) -1;
				} catch (IOException ioe) {

					throw new RuntimeException(ioe);
				}
				i++;
			} while (idCliente < 0);

			cliente = us.getClienteById(idCliente);

		} while (cliente == null);

		Evento evento = new Evento(name, fotografo_temporal, 0, cliente);

		evento = Factories.services.getFotografoServices().eventoCreate(evento);

		Printer.print("El evento ha sido creado con exito.");

		Printer.print("A continuación tendrá que agregar las fotos: "
				+ "(Escriba 'N' para finalizar la insercción de fotografias.)");

		String finalizar = "";
		String nombre_fotografia = "";

		while (!finalizar.equals("N")) {
			nombre_fotografia = Util.getString("Nombre foto: ");

			Fotografia fotografia = new Fotografia(nombre_fotografia, null,
					evento);

			Factories.services.getFotografoServices().fotografiaCreate(
					fotografia);

			finalizar = Util
					.getString("Desea introducir otra fotografia en el evento"
							+ " (Y/N): ");

			while ((!finalizar.equals("N")) & (!finalizar.equals("Y"))) {
				finalizar = Util
						.getString("Por favor seleccione una de las dos "
								+ "opciones (Y/N): ");
			}
		}
	}
}
