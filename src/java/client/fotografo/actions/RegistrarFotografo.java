package client.fotografo.actions;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Direccion;
import uo.spo.photo.model.Fotografo;
import uo.spo.photo.model.Usuario;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos dar de alta
 * nuevos fotografos que puedan utilizar y administrar la aplicacion.
 * 
 * @author Angel
 * 
 * */
public class RegistrarFotografo {

	public void execute() throws Exception {

		if (ClientSession.getTipousuario() == 2) {
			String nombre = Util.getString("Nombre de fotografo: ");
			String apellidos = Util.getString("Apellidos: ");
			String login = Util.getString("Login: ");
			String pass = Util.getString("Password: ");
			String zip = Util.getString("Zip: ");
			String calle = Util.getString("Calle: ");
			String ciudad = Util.getString("Ciudad: ");
			String email = Util.getString("Email: ");

			Direccion direccion = new Direccion(calle, zip, ciudad);

			Usuario usuario = new Fotografo(nombre, apellidos, login, pass,
					email, false, direccion);

			Factories.services.getUsuarioRegistradoServices().userRegisterNew(
					usuario);

		} else {
			Printer.print("No eres fotografo no puedes registrar a nadie.");

		}

	}

}
