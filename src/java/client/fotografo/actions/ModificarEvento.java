package client.fotografo.actions;

import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Evento;
import client.util.AbstractAction;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos modificar los
 * datos que representan a un evento creado en la aplicacion.
 * 
 * Tras la comprobacion previa, de la existencia del evento que se desea
 * modificar, se realiza la modificacion.
 * 
 * @author Angel
 * 
 * */
public class ModificarEvento extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Long idEvento = Util.getLong("Id evento a modificar: ");

		FotografoServices us = Factories.services.getFotografoServices();

		Evento evento = us.getEventoById(idEvento);

		while (evento == null) {
			idEvento = Util.getLong("Introduce un id de evento valido para"
					+ " modificar: ");
			evento = us.getEventoById(idEvento);
		}

		String nombre = Util.getString("Nombre nuevo: ");

		evento.setNombre(nombre);

		us.eventoUpdate(evento);
	}
}
