package client.fotografo.actions;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import client.util.AbstractAction;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos eliminar
 * albumes.
 * 
 * Simplemente facilitando el ID del album que se desea eliminar, se eliminará
 * su referencia en la tabla de la base de datos.
 * 
 * @author Angel
 * 
 * */
public class EliminarAlbum extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Long idAlbum = Util.getLong("Id album a eliminar: ");

		ClienteServices us = Factories.services.getClienteServices();

		Album album = us.getAlbumById(idAlbum);

		while (album == null) {
			idAlbum = Util
					.getLong("Introduce un id de evento valido para eliminar: ");
			album = us.getAlbumById(idAlbum);
		}

		us.albumDelete(album);
	}
}
