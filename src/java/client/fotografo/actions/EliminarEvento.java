package client.fotografo.actions;

import uo.spo.photo.business.FotografoServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Evento;
import client.util.AbstractAction;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como fotografos eliminar
 * eventos.
 * 
 * Simplemente facilitando el ID del evento que se desea eliminar, se eliminará
 * su referencia en la tabla de la base de datos.
 * 
 * @author Angel
 * 
 * */
public class EliminarEvento extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Long idEvento = Util.getLong("Id evento a eliminar: ");

		FotografoServices us = Factories.services.getFotografoServices();

		Evento evento = us.getEventoById(idEvento);

		while (evento == null) {
			idEvento = Util
					.getLong("Introduce un id de evento valido para eliminar: ");
			evento = us.getEventoById(idEvento);
		}

		us.eventoDelete(evento);
	}
}
