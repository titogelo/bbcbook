package client.usuario;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes listar todos los
 * eventos que han sido asignados al cliente que se encuentra en sesion en este
 * momento.
 * 
 * Obteniendo el ID del cliente de la sesion, simplemente tenemos que hacer una
 * consulta de que eventos han sido relacionados con ese cliente. Esa relacion
 * la establece el fotografo de turno cuando crea el evento.
 * 
 * @author Angel
 * 
 * */
public class ListarEventosByCliente extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long idCliente = ClientSession.getLoginId();

		ClienteServices us = Factories.services.getClienteServices();

		Cliente temporal = us.getClienteById(idCliente);

		while (temporal == null) {
			idCliente = Util
					.getLong("Por favor, introduzca un ID de cliente válido: ");

			temporal = us.getClienteById(idCliente);
		}

		Printer.print("Los evento creados para el señor "
				+ temporal.getApellidos() + " hasta el momento son: ");

		Cliente cliente = us.getEventoByCliente(idCliente);

		for (Evento evento : cliente.getEventos()) {
			Printer.print(" - " + evento.toString());
		}

	}
}
