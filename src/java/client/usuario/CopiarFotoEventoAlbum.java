package client.usuario;

import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Evento;
import uo.spo.photo.model.Fotografia;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes transferir una
 * foto, que un fotografo haya creado en un evento, a un album de su propiedad.
 * 
 * El proceso, incluye todas las comprobaciones pertinentes y basicamente lo que
 * hace es ofrecer una lista de fotografias que son susceptibles de ser
 * transferidas junto con su ID para que el cliente inserte el ID de la foto que
 * quiere copiar en el album que indico con anterioridad. Este proceso se
 * repetirá hasta que el cliente considere oportuno.
 * 
 * @author Angel
 * 
 * */
public class CopiarFotoEventoAlbum extends AbstractAction {

	@Override
	public void execute() throws Exception {

		ClienteServices us = Factories.services.getClienteServices();
		Long idCliente = ClientSession.getLoginId();

		Cliente cliente_temporal = us.getClienteById(idCliente);

		Cliente cliente = us.getAlbumByCliente(idCliente);

		Printer.print(" --- Eventos de origen disponibles --- ");
		for (Evento evento : cliente.getEventos()) {
			Printer.print(" - " + evento.toString());
		}

		Printer.print(" --- Albums de destino disponibles --- ");
		for (Album album : cliente.getAlbums()) {
			Printer.print(" - " + album.toString());
		}

		Long idEvento = Util.getLong("Evento id: ");
		Long idAlbum = Util.getLong("Album id: ");

		if (cliente_temporal == null)
			throw new BusinessException("El cliente debe existir.");

		Evento evento_temporal = us.getEventoById(idEvento);

		if (evento_temporal == null)
			throw new BusinessException("El evento debe existir.");

		Album album_temporal = us.getAlbumById(idAlbum);

		if (album_temporal == null)
			throw new BusinessException("El album debe existir.");

		Printer.print("Iniciando el proceso de transferencia de imagenes del"
				+ " evento '" + evento_temporal.getNombre() + "'"
				+ " al album '" + album_temporal.getNombre() + "'");

		String finalizar = "";
		Long idFotografia;

		while (!finalizar.equals("N")) {
			Printer.print("Fotografias que puede copiar: ");

			Evento lista_fotografias = us.getFotosByEvento(idEvento);

			for (Fotografia fotografia : lista_fotografias.getFotografias()) {
				Printer.print(" - " + fotografia.getNombre() + " ("
						+ fotografia.getId() + ")");
			}

			idFotografia = Util
					.getLong("Identificador de la fotografia que desea "
							+ "copiar: ");

			us.copyFotoFromEventoToAlbum(idEvento, idFotografia, album_temporal);

			finalizar = Util
					.getString("Desea transferir otra fotografia del evento "
							+ "al album (Y/N): ");

			while ((!finalizar.equals("N")) & (!finalizar.equals("Y"))) {
				finalizar = Util
						.getString("Por favor seleccione una de las dos "
								+ "opciones (Y/N): ");
			}
		}

		Printer.print("El la fotografias han sido añadidas con exito.");
	}
}
