package client.usuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import uo.spo.photo.model.Fotografia;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes listar las
 * fotografias de los albums que han sido creados por el cliente que se
 * encuentra en sesion.
 * 
 * Aqui vamos un poco mas allá y mostramos ademas de los albums del cliente
 * todas las fotografias que pertenecen a dichos albums y ademas teniendo en
 * cuenta el album al que pertenecen
 * 
 * @author Angel
 * 
 * */
public class ListarFotografiasAlbumsByCliente extends AbstractAction {

	protected BufferedReader kbd = new BufferedReader(new InputStreamReader(
			System.in));

	@Override
	public void execute() throws Exception {

		Long idCliente = ClientSession.getLoginId();

		ClienteServices us = Factories.services.getClienteServices();

		Cliente temporal = us.getClienteById(idCliente);

		while (temporal == null) {
			idCliente = Util
					.getLong("Por favor, introduzca un ID de cliente válido: ");

			temporal = us.getClienteById(idCliente);
		}

		Printer.print("Estos son sus albumes: ");

		Cliente cliente = us.getAlbumByCliente(idCliente);

		for (Album album : cliente.getAlbums()) {
			Printer.print(" - " + album.toString());
		}

		Long idAlbum;
		Album album = null;

		System.out
				.print("Seleccione el id del album del cual desea visualizar "
						+ "sus fotografias: ");

		do {
			do {

				try {
					idAlbum = (long) Integer.parseInt(kbd.readLine());
				} catch (NumberFormatException nfe) {
					System.out.print("Intentelo de nuevo: ");
					idAlbum = (long) -1;
				} catch (IOException ioe) {
					System.out.print("Intentelo de nuevo: ");
					throw new RuntimeException(ioe);
				}
			} while (idAlbum < 0);

			album = us.getAlbumById(idAlbum);

		} while (album == null);

		album = us.getFotosByAlbum(idAlbum);

		for (Fotografia fotografia : album.getFotografia()) {
			Printer.print(fotografia.toString());
		}
	}
}
