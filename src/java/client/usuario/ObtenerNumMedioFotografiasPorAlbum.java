package client.usuario;

import uo.spo.photo.infrastructure.Factories;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * 
 * 
 * @author Angel
 * 
 * */
public class ObtenerNumMedioFotografiasPorAlbum extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long num_medio_fotogragias = Factories.services.getClienteServices()
				.obtenerNumMedioFotografiasPorAlbum();

		Printer.print("La media de fotografías por album es: "
				+ num_medio_fotogragias);

	}

}
