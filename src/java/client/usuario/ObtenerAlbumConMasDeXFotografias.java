package client.usuario;

import java.util.List;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.AlbumFilterPhoto;
import client.util.AbstractAction;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes filtrar los
 * albums que existen en la aplicacion y obtener aquellos que poseen más de un
 * determinado numero de fotos que será especificado por el cliente en el
 * momento de la consulta.
 * 
 * Se trata de una consulta que abarca varias tablas y ademas utiliza las
 * agrupaciones. Las tablas que abarca son la de fotografias, la de albums y la
 * que relaciona ambas. En cuanto a las agrupaciones, las utilizo para contar el
 * numero de fotos que tiene un album; Una vez que ya tengo el numero de
 * fotografias por album utilizando un having de SQL en la consulta muestro solo
 * aquellos albums que superen el numero de fotos facilitado por el cliente.
 * 
 * Bien es verdad que la funcionalidad que ofrece esta clase no es muy util en
 * una aplicacion real, pero la he implementado para cumplir con los requisitos
 * minimos del enunciado.
 * 
 * @author Angel
 * 
 * */
public class ObtenerAlbumConMasDeXFotografias extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long num_fotografias = new Long(0), pasadas = new Long(0);
		ClienteServices us = Factories.services.getClienteServices();

		while (num_fotografias == 0) {
			String por_favor = "";
			if (pasadas >= 1) {
				por_favor = "Por favor, ";
			}
			num_fotografias = Util.getLong(por_favor
					+ "Introduzca el número de fotos por "
					+ "el cual desea filtrar los albumes: ");
			pasadas++;
		}

		List<AlbumFilterPhoto> lista = us
				.albumsByNumFotografias(num_fotografias);

		if (!lista.isEmpty()) {
			Printer.print("A continuación se van a listar los albums que "
					+ "poseen " + num_fotografias + " o mas fotos.");

			for (AlbumFilterPhoto albumFilterPhoto : lista) {
				Printer.print("Album: '" + albumFilterPhoto.getNombreAlbum()
						+ "' Fotos: "
						+ albumFilterPhoto.getNumeroDeFotografias());
			}

		} else {
			Printer.print("No existen albums con " + num_fotografias
					+ " o más fotos.");
		}

	}

}
