package client.usuario;

import uo.spo.photo.business.BusinessException;
import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes crea un nuevo
 * album pudiendo facilitar los datos identificativos de dicho album.
 * 
 * @author Angel
 * 
 * */
public class CreateAlbum extends AbstractAction {

	@Override
	public void execute() throws Exception {
		String name = Util.getString("Nombre: ");
		Long idCliente = ClientSession.getLoginId();

		ClienteServices us = Factories.services.getClienteServices();

		Cliente cliente_temporal = us.getClienteById(idCliente);

		if (cliente_temporal == null)
			throw new BusinessException("El cliente debe existir.");

		Album album = new Album(name, cliente_temporal, 0);

		album = Factories.services.getClienteServices().albumCreate(album);

		Printer.print("El album ha sido creado con exito.");

		Printer.print("Debe utilizar la opción 13 del menu.");
	}
}
