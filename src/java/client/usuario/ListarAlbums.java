package client.usuario;

import java.util.List;

import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import client.util.AbstractAction;
import client.util.Printer;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes listar todos los
 * albumes que han sido creados por los distintos clientes.
 * 
 * Ojo unicamente es posible listarlos, es decir no hay manera de acceder a sus
 * fotografias.
 * 
 * @author Angel
 * 
 * */
public class ListarAlbums extends AbstractAction {

	@Override
	public void execute() throws Exception {
		Printer.print("Todos los albums creados hasta el momento son: ");

		List<Album> lista = Factories.services.getClienteServices()
				.getAllAlbums();

		for (Album album : lista) {
			Printer.print(" - " + album.toString());
		}
	}
}
