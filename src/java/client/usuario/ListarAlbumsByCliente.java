package client.usuario;

import uo.spo.photo.business.ClienteServices;
import uo.spo.photo.infrastructure.Factories;
import uo.spo.photo.model.Album;
import uo.spo.photo.model.Cliente;
import client.util.AbstractAction;
import client.util.ClientSession;
import client.util.Printer;
import client.util.Util;

/**
 * 
 * Esta clase permite a los usuarios registrados como clientes listar todos los
 * albumes que han sido creados por el cliente que se encuentra en sesion en
 * este momento.
 * 
 * Obteniendo el ID del cliente de la sesion, simplemente tenemos que hacer una
 * consulta de que albums son de su propiedad
 * 
 * @author Angel
 * 
 * */
public class ListarAlbumsByCliente extends AbstractAction {

	@Override
	public void execute() throws Exception {

		Long idCliente = ClientSession.getLoginId();

		ClienteServices us = Factories.services.getClienteServices();

		Cliente temporal = us.getClienteById(idCliente);

		while (temporal == null) {
			idCliente = Util
					.getLong("Por favor, introduzca un ID de cliente válido: ");

			temporal = us.getClienteById(idCliente);
		}

		Printer.print("Los albums creados por el señor "
				+ temporal.getApellidos() + " hasta el momento son: ");

		Cliente cliente = us.getAlbumByCliente(idCliente);

		for (Album album : cliente.getAlbums()) {
			Printer.print(" - " + album.toString());
		}
	}
}
